/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Role.NurseRole;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author Swarna
 */
public abstract class OrganizationSubclass extends Organization {

    public OrganizationSubclass(String name) {
        super(name);
        roles.add(new NurseRole());
    }
}
