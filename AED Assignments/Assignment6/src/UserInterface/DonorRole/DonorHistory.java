/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.DonorRole;

import Business.Employee.Employee;
import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class DonorHistory extends Employee{
    
    ArrayList<Donor> donorList;
    
    public DonorHistory()
    {
        donorList = new ArrayList<>();
    }

    public ArrayList<Donor> getRequest() {
        return donorList;
    }

    public void setRequest(ArrayList<Donor> request) {
        this.donorList = request;
    }
    
    public Donor addRequest()
    {
        Donor lt = new Donor();
        donorList.add(lt);
        return lt;
    }
    
    
}
