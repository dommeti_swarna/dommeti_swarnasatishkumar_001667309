/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.DonorRole;

import Business.Employee.Employee;

/**
 *
 * @author Swarna
 */
public class Donor{
    
    private String bloodType;
    private String dateofDonation;
    private String barcode;
    private String pintOfBlood;
    
  

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getDateofDonation() {
        return dateofDonation;
    }

    public void setDateofDonation(String dateofDonation) {
        this.dateofDonation = dateofDonation;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getPintOfBlood() {
        return pintOfBlood;
    }

    public void setPintOfBlood(String pintOfBlood) {
        this.pintOfBlood = pintOfBlood;
    }

    
    
    
      
}
