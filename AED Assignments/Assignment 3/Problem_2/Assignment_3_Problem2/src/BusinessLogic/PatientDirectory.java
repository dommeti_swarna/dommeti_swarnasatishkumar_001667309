/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class PatientDirectory{
    
    private ArrayList<Patient> patientDirectory;
    Patient patient = new Patient();
    
    public PatientDirectory()
    {
        patientDirectory = new ArrayList<>();
                
    }

    public ArrayList<Patient> getPatientDirectory() {
        return patientDirectory;
    }

    public void setPatientDirectory(ArrayList<Patient> patientDirectory) {
        this.patientDirectory = patientDirectory;
    }
    
    public Patient addPatient()
    {
        Patient patient = new Patient();
        patientDirectory.add(patient);
        return patient;
    }
    
    public void deletePatient(Patient pp)
    {
        patientDirectory.remove(pp);    
    }
    public Patient searchPatient(int patientID)
    {
        for(Patient p : patientDirectory)
        {
            if(p.equals(p.getPatientID()))
            {
                return p;
            }
        }
        return null;
    }
}
