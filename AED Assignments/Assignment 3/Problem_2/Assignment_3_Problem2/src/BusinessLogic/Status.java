/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author Swarna
 */
public class Status {
    
    private Patient patient;
    private Person person;
    
    public Status(Patient patient,Person person)
    {
        this.patient = patient;
        this.person = person;
    }
    public String getStatus()
    {
        String status;
        if((person.getAge()>=1 && person.getAge()<=3)&&(patient.getRespiratoryRate()>=20 && patient.getRespiratoryRate()<=30)&&(patient.getHeartRate()>=80 && patient.getHeartRate()<=130)&&(patient.getSystolicBloodPressure()>=80 && patient.getSystolicBloodPressure()<=110)&&(patient.getWeight()>=22 && patient.getWeight()<=31))
            {
                status = "Normal";    
                return status;
            }
            else if((person.getAge()>=4 && person.getAge()<=5)&&(patient.getRespiratoryRate()>=20 && patient.getRespiratoryRate()<=30)&&(patient.getHeartRate()>=80 && patient.getHeartRate()<=120)&&(patient.getSystolicBloodPressure()>=80 && patient.getSystolicBloodPressure()<=110)&&(patient.getWeight()>=31 && patient.getWeight()<=40))
            {
                status = "Normal";     
                return status;
            }
            else if((person.getAge()>=6 && person.getAge()<=12)&&(patient.getRespiratoryRate()>=20 && patient.getRespiratoryRate()<=30)&&(patient.getHeartRate()>=70 && patient.getHeartRate()<=110)&&(patient.getSystolicBloodPressure()>=80 && patient.getSystolicBloodPressure()<=120)&&(patient.getWeight()>=41 && patient.getWeight()<=92))
            {
                status = "Normal";
                return status;
            }
           else if((person.getAge()>=13)&&(patient.getRespiratoryRate()>=12 && patient.getRespiratoryRate()<=20)&&(patient.getHeartRate()>=55 && patient.getHeartRate()<=105)&&(patient.getSystolicBloodPressure()>=110 && patient.getSystolicBloodPressure()<=120)&&(patient.getWeight()>110)){
               
              status = "Normal";   
               return status;
            }
            else
            {
                status = "Abnormal";
               return status;
            }           
    }
            
}
    

