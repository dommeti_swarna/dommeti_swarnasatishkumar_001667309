/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class SupplierDirectory {
    

    
    private ArrayList<Supplier> supplierList = new ArrayList<>();
    
  
    public SupplierDirectory(){
        
    }
    
    public SupplierDirectory(ArrayList<Supplier> supplierList){

        this.supplierList = supplierList;     
    }

    public ArrayList<Supplier> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(ArrayList<Supplier> supplierList) {
        this.supplierList = supplierList;
    }
    
    
    
    @Override
    public String toString() {
        return " " + supplierList ;
    }

    
    }
    