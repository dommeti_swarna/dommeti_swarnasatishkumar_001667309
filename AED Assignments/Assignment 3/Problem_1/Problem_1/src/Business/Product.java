/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Swarna
 */
public class Product {
    
    private String name;
    private String price;
    private String operatingSystem;
    private String processor;
    
    public Product(String name, String price,String operatingSystem,String processor){
        this.name = name;
        this.price = price;
        this.operatingSystem = operatingSystem;
        this.processor = processor;
    }
    
    @Override
    public String toString(){
         return " \n Name: "+ name + " \n " + "Price: " + price + "\n" + "Operating System: " + operatingSystem + "\n" + "Memory:" + processor + "\n";  
    }
}
