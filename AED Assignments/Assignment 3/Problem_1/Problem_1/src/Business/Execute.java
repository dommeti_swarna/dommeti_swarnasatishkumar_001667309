/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class Execute {
    
    public static void main(String args[])
    {        
          
        SupplierDirectory supplierDirectory = new SupplierDirectory();
        
        Supplier dell = new Supplier("Dell");
        Supplier lenovo = new Supplier("Lenovo");
        Supplier toshiba = new Supplier("Toshiba");
        Supplier hp = new Supplier("HP");
        Supplier apple = new Supplier("Apple");
        
        ArrayList<Product> dellProducts = new ArrayList<>();
        dellProducts.add(new Product("XPS1","600.00","Windows 8","Processor i3"));
        dellProducts.add(new Product("XPS2","700.00","Windows 8","Processor i5"));
        dellProducts.add(new Product("XPS3","800.00","Windows 10","Processor i7"));
        dellProducts.add(new Product("Inspiron1","650.00","Windows 8","Processor i3"));
        dellProducts.add(new Product("Inspiron2","670.00","Windows 10","Processor i5"));
        dellProducts.add(new Product("Inspiron3","350.00","Windows 8","Processor i7"));
        dellProducts.add(new Product("Ultra1","400.00","Windows 10","Processor i5"));
        dellProducts.add(new Product("Ultra2","500.00","Windows 10","Processor i7"));
        dellProducts.add(new Product("Ultra Pro 1","700.00","Windows 8","Processor i5"));
        dellProducts.add(new Product("Ultra Pro 2","600.00","Windows 8","Processor i7"));
        
        ProductCatalog dellCatalog = new ProductCatalog(dellProducts);
        dell.setProductCatalog(dellCatalog);
        
        ArrayList<Product> lenovoProducts = new ArrayList<>();
        lenovoProducts.add(new Product("G50","650.00","Windows 8","Processor i7"));
        lenovoProducts.add(new Product("G51","750.00","Windows 8","Processor i7"));
        lenovoProducts.add(new Product("Z50","550.00","Windows 10","Processor i5"));
        lenovoProducts.add(new Product("Z55","700.00","Windows 8","Processor i7"));
        lenovoProducts.add(new Product("S510P","760.00","Windows 8","Processor i7"));
        lenovoProducts.add(new Product("G45","700.00","Windows 8","Processor i5"));
        lenovoProducts.add(new Product("G35","780.00","Windows 8","Processor i3"));
        lenovoProducts.add(new Product("S550R","600.00","Windows 8","Processor i7"));
        lenovoProducts.add(new Product("Z45","700.00","Windows 8","Processor i5"));
        lenovoProducts.add(new Product("G40","500.00","Windows 8","Processor i3"));
        
        ProductCatalog lenovoCatalog = new ProductCatalog(lenovoProducts);
        lenovo.setProductCatalog(lenovoCatalog);
        
        
        ArrayList<Product> appleProducts = new ArrayList<>();
        appleProducts.add(new Product("MacBook","1000.00","iOS","8GB"));
        appleProducts.add(new Product("MacBook Pro","1200.00","iOS","8GB"));
        appleProducts.add(new Product("iPhone 5","800.00","iOS","Processor A7"));
        appleProducts.add(new Product("iPhone 5S","900.00","iOS","Processor  A8"));
        appleProducts.add(new Product("iPhone 6","950.00","iOS","Processor A9"));
        appleProducts.add(new Product("iPhone 6S","1050.00","iOS","Processor A9"));
        appleProducts.add(new Product("iPad 2","600.00","iOS","Processor A7"));
        appleProducts.add(new Product("iPad 3","700.00","iOS","Processor A8"));
        appleProducts.add(new Product("iPad Mini 1","780.00","iOS","Processor A8"));
        appleProducts.add(new Product("iPad Mini 2","800.00","iOS","Processor A9"));
        
        ProductCatalog appleCatalog = new ProductCatalog((appleProducts));
        apple.setProductCatalog(appleCatalog);
        
        ArrayList<Product> hpProducts = new ArrayList<>();
        hpProducts.add(new Product("HP1","500.00","Windows 8","Processor i3"));
        hpProducts.add(new Product("HP2","700.00","Windows 10","Processor i3"));
        hpProducts.add(new Product("HP3","670.00","Windows 8","Processor i3"));
        hpProducts.add(new Product("HP4","450.00","Windows 8","Processor i5"));
        hpProducts.add(new Product("HP5","700.00","Windows 10","Processor i5"));
        hpProducts.add(new Product("HP6","780.00","Windows 8","Processor i5"));
        hpProducts.add(new Product("HP7","890.00","Windows 8","Processor i7"));
        hpProducts.add(new Product("HP8","780.00","Windows 8","Processor i7"));
        hpProducts.add(new Product("HP9","890.00","Windows 8","Processor i7"));
        hpProducts.add(new Product("HP10","900.00","Windows 8","AMD A10"));
        
        ProductCatalog hpCatalog = new ProductCatalog(hpProducts);
        hp.setProductCatalog(hpCatalog);
        
        ArrayList<Product> toshibaProducts = new ArrayList<>();
        toshibaProducts.add(new Product("Toshiba 1","500.00","Windows 8","Processor i3"));
        toshibaProducts.add(new Product("Toshiba 2","450.00","Windws 10","Processor i5"));
        toshibaProducts.add(new Product("Toshiba 3","600.00","Windows 8","Processor i7"));
        toshibaProducts.add(new Product("Toshiba 4","700.00","Windows 8","Processor i3"));
        toshibaProducts.add(new Product("Toshiba 5","550.00","Windows 10","Processor i5"));
        toshibaProducts.add(new Product("Toshiba 6","350.00","Windows 8","Processor i7"));
        toshibaProducts.add(new Product("Toshiba 7","500.00","Windows 8","Processor i3"));
        toshibaProducts.add(new Product("Toshiba 8","600.00","Windows 10","Processor i5"));
        toshibaProducts.add(new Product("Toshiba 9","700.00","Windows 8","Processor i7"));
        toshibaProducts.add(new Product("Toshiba 10","800.00","Windows 8","Processor i3"));
        
        ProductCatalog toshibaCatalog = new ProductCatalog(toshibaProducts);
        toshiba.setProductCatalog(toshibaCatalog);
      
        supplierDirectory.getSupplierList().add(dell);
        supplierDirectory.getSupplierList().add(lenovo);
        supplierDirectory.getSupplierList().add(toshiba);
        supplierDirectory.getSupplierList().add(hp);
        supplierDirectory.getSupplierList().add(apple);
            
        System.out.println(supplierDirectory.toString().replace(",","").replace("[","").replace("]",""));        
    }
}
