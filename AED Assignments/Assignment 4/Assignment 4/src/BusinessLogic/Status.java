/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author Swarna
 */
public class Status {
    
    private Drug drug;
    private Store store;
    private DrugCatalog drugCatalog;
    private int threshold = 10;
    String below = "Quantity is below threshold";
    String above = "Quantity is above threshold";
    public Status(DrugCatalog drugCatalog,Drug drug)
    {
        this.drugCatalog = drugCatalog;
        this.drug = drug;
        validate();
        
    }
    
    public String validate()
    {
        if(drug.getDrugQuantity()< threshold)
        {
            return below ;
        }
        else
        {
            return above;
        }
    }
    
}
