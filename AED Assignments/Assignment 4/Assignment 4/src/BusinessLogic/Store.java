/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.Date;

/**
 *
 * @author Swarna
 */
public class Store {
    
   private String storeName;
   private DrugCatalog drugCatalog;
   
   public Store()
   {
       drugCatalog =  new DrugCatalog();
   }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public DrugCatalog getDrugCatalog() {
        return drugCatalog;
    }

    public void setDrugCatalog(DrugCatalog drugCatalog) {
        this.drugCatalog = drugCatalog;
    }
    
    public String toString()
    {
        return storeName;
    }
}
