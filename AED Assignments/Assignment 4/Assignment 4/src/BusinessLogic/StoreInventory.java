/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author Swarna
 */
public class StoreInventory {
    
    private DrugCatalog drugCatalog;
    private Store store;
    public StoreInventory(Store store)
    {
        Drug drug1 = store.getDrugCatalog().addDrug();
        drug1.setDrugNo(100);
        drug1.setDrugName("Crocin");
        drug1.setDrugQuantity(10);
        drug1.setDrugType("Releive body pain");
        drug1.setDrugCategory("Painkiller");
        drug1.setDrugExpiry("10/10/2018");
        drug1.setManufacturerNumber(200);
        drug1.setDrugManufacturer("Glaxosmith");
        drug1.setDrugListPrice(30);
    
        Drug drug2 = store.getDrugCatalog().addDrug();
        drug2.setDrugNo(101);
        drug2.setDrugName("Allegra");
        drug2.setDrugQuantity(15);
        drug2.setDrugType("Allergies");
        drug2.setDrugCategory("Removing allergies");
        drug2.setDrugExpiry("11/25/2018");
        drug2.setManufacturerNumber(201);
        drug2.setDrugManufacturer("Allergin");
        drug2.setDrugListPrice(25);
        
        Drug drug3 = store.getDrugCatalog().addDrug();
        drug3.setDrugNo(102);
        drug3.setDrugName("Domstal");
        drug3.setDrugQuantity(20);
        drug3.setDrugType("Vommiting");
        drug3.setDrugCategory("Vommiting");
        drug3.setDrugExpiry("09/20/2018");
        drug3.setManufacturerNumber(203);
        drug3.setDrugManufacturer("Vomit");
        drug3.setDrugListPrice(15);
        
        Drug drug4 = store.getDrugCatalog().addDrug();
        drug4.setDrugNo(103);
        drug4.setDrugName("Combiflam");
        drug4.setDrugQuantity(20);
        drug4.setDrugType("Relieving headache");
        drug4.setDrugCategory("Pain killer");
        drug4.setDrugExpiry("08/28/2018");
        drug4.setManufacturerNumber(204);
        drug4.setDrugManufacturer("Combiflam");
        drug4.setDrugListPrice(20);
        
        Drug drug5 = store.getDrugCatalog().addDrug();
        drug5.setDrugNo(104);
        drug5.setDrugName("Romatc");
        drug5.setDrugQuantity(20);
        drug5.setDrugType("Mild Acidity");
        drug5.setDrugCategory("Rantac");
        drug5.setDrugExpiry("08/25/2019");
        drug5.setManufacturerNumber(205);
        drug5.setDrugManufacturer("Acidity");
        drug5.setDrugListPrice(10);
    }
}
