/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.Date;

/**
 *
 * @author Swarna
 */
public class Drug {
    
    private int drugNo;
    private String drugName;
    private int drugQuantity;
    private String drugManufacturer;
    private int drugListPrice;
    private String drugCategory;
    private String drugType;
    private String manufacturerLocation;
    private int manufacturerNumber;
    private String drugExpiry;
    private String drugManu;
    static int counter;
    
   
    public String getCvsStoreLocation() {
        return cvsStoreLocation;
    }

    public void setCvsStoreLocation(String cvsStoreLocation) {
        this.cvsStoreLocation = cvsStoreLocation;
    }
    private String cvsStoreLocation;

    public int getDrugNo() {
        return drugNo;
    }

    public void setDrugNo(int drugNo) {
        this.drugNo = drugNo;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public int getDrugQuantity() {
        return drugQuantity;
    }

    public void setDrugQuantity(int drugQuantity) {
        this.drugQuantity = drugQuantity;
    }

    public String getDrugManufacturer() {
        return drugManufacturer;
    }

    public void setDrugManufacturer(String drugManufacturer) {
        this.drugManufacturer = drugManufacturer;
    }

    public int getDrugListPrice() {
        return drugListPrice;
    }

    public void setDrugListPrice(int drugListPrice) {
        this.drugListPrice = drugListPrice;
    }

    public String getDrugCategory() {
        return drugCategory;
    }

    public void setDrugCategory(String drugCategory) {
        this.drugCategory = drugCategory;
    }

    public String getDrugExpiry() {
        return drugExpiry;
    }

    public void setDrugExpiry(String drugExpiry) {
        this.drugExpiry = drugExpiry;
    }

    public String getDrugManu() {
        return drugManu;
    }

    public void setDrugManu(String drugManu) {
        this.drugManu = drugManu;
    }


    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getManufacturerLocation() {
        return manufacturerLocation;
    }

    public void setManufacturerLocation(String manufacturerLocation) {
        this.manufacturerLocation = manufacturerLocation;
    }

    public int getManufacturerNumber() {
        return manufacturerNumber;
    }

    public void setManufacturerNumber(int manufacturerNumber) {
        this.manufacturerNumber = manufacturerNumber;
    }
    
    public String toString()
    {
        return String.valueOf(drugNo);
    }
    
    
}
