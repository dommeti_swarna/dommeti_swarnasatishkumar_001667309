/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class StoreCatalog {
    
    private ArrayList<Store> storeCatalogList;
    
    public StoreCatalog()
    {
        storeCatalogList = new ArrayList<>();
    }

    public ArrayList<Store> getStoreCatalogList() {
        return storeCatalogList;
    }

    public void setStoreCatalogList(ArrayList<Store> storeCatalogList) {
        this.storeCatalogList = storeCatalogList;
    }
    
    public Store addStoreDrug()
    {
        Store sc = new Store();
        storeCatalogList.add(sc);
        return sc;
    }
    
    public void removeStoreDrug(Store store)
    {
        storeCatalogList.remove(store);
    }
    
    public String searchStoreDrug(String storeName)
    {
        for(Store store: storeCatalogList)
        {
            if(store.getStoreName()==storeName)
            {
                return storeName;
            }
        }
        return null;
    }
    
  
            
    
}
