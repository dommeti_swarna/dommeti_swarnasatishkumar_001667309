/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class DrugCatalog {
    
    private ArrayList<Drug> drugCatalogList;
    
    public DrugCatalog()
    {
        drugCatalogList = new ArrayList<Drug>();
    }

   
      
    public Drug addDrug()
    {
        Drug dc = new Drug();
        drugCatalogList.add(dc);
        return dc;
    }
    
    public void removeDrug(Drug drug)
    {
        drugCatalogList.remove(drug);
    }
    
    public String searchDrugExpiry(String expiryDate)
    {
        for(Drug drugCatalog :drugCatalogList)
        {
            if(drugCatalog.getDrugExpiry().equals(expiryDate))
            {
                return expiryDate;
            }
        }
        return null;
    }
    
    public int searchDrugID(int serialID)
    {
        for(Drug drugCatalog :drugCatalogList)
        {
            if((drugCatalog.getDrugNo())==serialID)
            {
                return serialID;
            }
        }
        return 0;
    }
    
    public Drug searchDrug(int serialID)
    {
        for(Drug drug :drugCatalogList)
        {
            if((drug.getDrugNo())==serialID)
            {
                return drug;
            }
        }
        return null;
    }

    public ArrayList<Drug> getDrugCatalogList() {
        return drugCatalogList;
    }

    public void setDrugCatalogList(ArrayList<Drug> drugCatalogList) {
        this.drugCatalogList = drugCatalogList;
    }
    
      public int searchDrugQuantity(int quantity)
    {
        for(Drug drug:drugCatalogList)
        {
            if(drug.getDrugQuantity()==quantity)
            {
                return quantity;
            }
            
        }
        return 0;
    }
      
}
