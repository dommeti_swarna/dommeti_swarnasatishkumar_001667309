/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author Swarna
 */
public class Inventory {
    
    public static int Crocin = 500;
    public static int Allegra = 400;
    public static int Domstal = 500;
    public static int Combiflam = 400;
    public static int Romatc = 500;

    public static int getCrocin() {
        return Crocin;
    }

    public static void setCrocin(int Crocin) {
        Inventory.Crocin = Crocin;
    }

    public static int getAllegra() {
        return Allegra;
    }

    public static void setAllegra(int Allegra) {
        Inventory.Allegra = Allegra;
    }

    public static int getDomstal() {
        return Domstal;
    }

    public static void setDomstal(int Domstal) {
        Inventory.Domstal = Domstal;
    }

    public static int getCombiflam() {
        return Combiflam;
    }

    public static void setCombiflam(int Combiflam) {
        Inventory.Combiflam = Combiflam;
    }

    public static int getRomatc() {
        return Romatc;
    }

    public static void setRomatc(int Romatc) {
        Inventory.Romatc = Romatc;
    }
              
    
}
