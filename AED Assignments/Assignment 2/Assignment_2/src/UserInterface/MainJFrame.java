/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import BusinessLogic.Patient;
import BusinessLogic.VitalSign;
import BusinessLogic.VitalSignHistory;

/**
 *
 * @author Swarna
 */
public class MainJFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainJFrame
     */
    private VitalSignHistory vitalSignHistory;
    private Patient patient;
    private VitalSign vitalSign;
    
    
    public MainJFrame() 
    {
        initComponents();
        vitalSignHistory =  new VitalSignHistory();
        patient = new Patient();
        vitalSign = new VitalSign();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        createProfileBtn = new javax.swing.JButton();
        createVitalSignBtn = new javax.swing.JButton();
        viewVitalSignBtn = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        createProfileBtn.setText("Create Profile");
        createProfileBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createProfileBtnActionPerformed(evt);
            }
        });

        createVitalSignBtn.setText("Add Vital Sign");
        createVitalSignBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createVitalSignBtnActionPerformed(evt);
            }
        });

        viewVitalSignBtn.setText("View Profile");
        viewVitalSignBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewVitalSignBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(createProfileBtn)
                    .addComponent(createVitalSignBtn)
                    .addComponent(viewVitalSignBtn))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(createProfileBtn)
                .addGap(50, 50, 50)
                .addComponent(createVitalSignBtn)
                .addGap(51, 51, 51)
                .addComponent(viewVitalSignBtn)
                .addGap(85, 85, 85))
        );

        jSplitPane2.setLeftComponent(jPanel3);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 431, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 298, Short.MAX_VALUE)
        );

        jSplitPane2.setRightComponent(jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void createProfileBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createProfileBtnActionPerformed
        CreatePatientDetails pd = new CreatePatientDetails(patient);
        jSplitPane2.setRightComponent(pd);
        createProfileBtn.setEnabled(false);
        viewVitalSignBtn.setEnabled(false);
    }//GEN-LAST:event_createProfileBtnActionPerformed

    private void createVitalSignBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createVitalSignBtnActionPerformed
        CreateVitalSign cvs = new CreateVitalSign(vitalSignHistory);
        jSplitPane2.setRightComponent(cvs);
        viewVitalSignBtn.setEnabled(true);
    }//GEN-LAST:event_createVitalSignBtnActionPerformed

    private void viewVitalSignBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewVitalSignBtnActionPerformed
        ViewVitalSign vvs = new ViewVitalSign(vitalSignHistory,patient,vitalSign);
        ViewPatientProfile vpp = new ViewPatientProfile(patient);
        jSplitPane2.setTopComponent(vpp);
        jSplitPane2.setBottomComponent(vvs);
    }//GEN-LAST:event_viewVitalSignBtnActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton createProfileBtn;
    private javax.swing.JButton createVitalSignBtn;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JButton viewVitalSignBtn;
    // End of variables declaration//GEN-END:variables
}
