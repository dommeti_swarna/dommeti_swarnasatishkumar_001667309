/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.SalespersonRole;

import BusinessLogic.Customer;
import BusinessLogic.CustomerCatalog;
import BusinessLogic.MasterOrderCatalog;
import BusinessLogic.Salesperson;
import BusinessLogic.SupplierDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Swarna
 */
public class SalespersonWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form SalespersonWorkAreaJPanel
     */
    private JPanel userProcessContainer;
    private Salesperson salesperson;
    private SupplierDirectory supplierDirectory;
    private CustomerCatalog customerCatalog;  
    private MasterOrderCatalog masterOrderCatalog;
    private Customer customer;
            
    
    public SalespersonWorkAreaJPanel(JPanel userProcessContainer,Salesperson salesperson,SupplierDirectory supplierDirectory,CustomerCatalog customerCatalog,MasterOrderCatalog masterOrderCatalog) {
        initComponents();
        this.userProcessContainer =  userProcessContainer;
        this.salesperson = salesperson;
        this.supplierDirectory = supplierDirectory;
        this.customerCatalog = customerCatalog;
        this.masterOrderCatalog = masterOrderCatalog;
     
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        managePButton1 = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("My Work Area (Salesperson Manager Role)");

        managePButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        managePButton1.setText("Manage Salesperson Catalog >>");
        managePButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                managePButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 509, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel1)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(90, 90, 90)
                    .addComponent(managePButton1)
                    .addContainerGap(130, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 17, Short.MAX_VALUE)
                    .addComponent(jLabel1)
                    .addGap(0, 254, Short.MAX_VALUE)))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(107, 107, 107)
                    .addComponent(managePButton1)
                    .addContainerGap(162, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void managePButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_managePButton1ActionPerformed
        // TODO add your handling code here:
        ManageCustomerCatalogJPanel mc = new ManageCustomerCatalogJPanel(userProcessContainer,supplierDirectory,salesperson,masterOrderCatalog,customerCatalog);
        userProcessContainer.add("Adding a customer",mc);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_managePButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton managePButton1;
    // End of variables declaration//GEN-END:variables
}
