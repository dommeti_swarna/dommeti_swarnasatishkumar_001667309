/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author Swarna
 */
public class Business {
    
    private MasterOrderCatalog masterOrderCatalog;
    private SalespersonCatalog salespersonCatalog;
    private CustomerCatalog customerCatalog;
    private SupplierDirectory supplierDirectory;
    
    public Business()
    {
        masterOrderCatalog = new MasterOrderCatalog();
        salespersonCatalog = new SalespersonCatalog();
        customerCatalog = new CustomerCatalog();
        supplierDirectory = new SupplierDirectory();
    }

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }

    public SalespersonCatalog getSalespersonCatalog() {
        return salespersonCatalog;
    }

    public void setSalespersonCatalog(SalespersonCatalog salespersonCatalog) {
        this.salespersonCatalog = salespersonCatalog;
    }

    public CustomerCatalog getCustomerCatalog() {
        return customerCatalog;
    }

    public void setCustomerCatalog(CustomerCatalog customerCatalog) {
        this.customerCatalog = customerCatalog;
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }
    
}
