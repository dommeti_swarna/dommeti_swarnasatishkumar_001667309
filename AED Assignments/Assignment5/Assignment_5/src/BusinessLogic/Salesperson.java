/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

/**
 *
 * @author Swarna
 */
public class Salesperson extends Person {
    
    private int salespersonID;
    private static int count=0;
    private double commission;
    private CustomerCatalog customerCatalog;
    
    
    public Salesperson()
    {
        count++;
        salespersonID = count;
        customerCatalog=new CustomerCatalog();
        
    }

    public int getSalespersonID() {
        return salespersonID;
    }

    public void setSalespersonID(int salespersonID) {
        this.salespersonID = salespersonID;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Salesperson.count = count;
    }

    public String toString()
    {
        return String.valueOf(salespersonID);
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public CustomerCatalog getCustomerCatalog() {
        return customerCatalog;
    }

    public void setCustomerCatalog(CustomerCatalog customerCatalog) {
        this.customerCatalog = customerCatalog;
    }
    
    
    
    
}
