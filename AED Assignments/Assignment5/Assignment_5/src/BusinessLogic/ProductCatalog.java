/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class ProductCatalog {
    
    private ArrayList<Product> productList;
    
    public ProductCatalog()
    {
        productList = new ArrayList<>();
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }
    
    public Product addProduct()
    {
        Product p = new Product();
        productList.add(p);
        return p;
    }
    
    public void removeProduct(Product p)
    {
        productList.remove(p);
    }
    
    public Product searchProduct(int p)
    {
        for(Product product : productList)
        {
            if(product.getModelNumber()==p)
            {
                return product;
            }
        }
        return null;
    }
    
   public Integer fetchProduct()
   {
       int size = productList.size();
       for(int i=0;i<size;i++)
       {
           Product product = (Product) productList.get(i);
           int t = product.getTargetPrice();
           return t;
       }
       return 0;
   }
}
