/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class SalespersonCatalog {
    
    private ArrayList<Salesperson> salespersonList;
    
    public SalespersonCatalog()
    {
        salespersonList = new ArrayList<Salesperson>();
    }

    public ArrayList<Salesperson> getSalespersonList() {
        return salespersonList;
    }

    public void setSalespersonList(ArrayList<Salesperson> salespersonList) {
        this.salespersonList = salespersonList;
    }
   
    public Salesperson addSalesperson()
    {
        Salesperson s = new Salesperson();
        salespersonList.add(s);
        return s;
    }
    
    public void removeSalesperson(Salesperson s)
    {
        salespersonList.remove(s);
    }
    
    public Salesperson searchSalesperson(int id)
    {
        for(Salesperson salesperson:salespersonList)
        {
            if(salesperson.getSalespersonID()==id)
            {
                return salesperson;
            }
        }
        return null;
    }
}
