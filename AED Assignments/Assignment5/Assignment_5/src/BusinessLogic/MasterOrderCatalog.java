/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class MasterOrderCatalog {
    
    private ArrayList<Order> orderList;
    
    public MasterOrderCatalog()
    {
        orderList = new ArrayList<>();
    }

    public ArrayList<Order> getMoc() {
        return orderList;
    }

    public void setMoc(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }
    
    public Order addOrder(Order order)
    {
        orderList.add(order);
        return order;
    }
    
    

    public ArrayList<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }
}
