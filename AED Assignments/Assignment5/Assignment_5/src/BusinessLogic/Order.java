/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class Order {
    
    private int orderID;
    private static int count=0;
    private ArrayList<OrderItems> orderItemList;
    private Customer customer;
    private Salesperson salesperson;
    
    
    public Order()
    {
        orderItemList = new ArrayList<OrderItems>();
        customer  =  new Customer();
        salesperson = new Salesperson();
        count++;
        orderID = count;
       
    }

    public ArrayList<OrderItems> getOrderList() {
        return orderItemList;
    }

    public void setOrderList(ArrayList<OrderItems> orderList) {
        this.orderItemList = orderList;
    }
    
    public OrderItems addOrderItem(Product p,int q,int s)
    {
        OrderItems oi = new OrderItems();
        oi.setProduct(p);
        oi.setQuantity(q);
        oi.setSalesPrice(s);
        
        
        
        orderItemList.add(oi);
        return oi;
                
    }
    public void removeOrderItem(OrderItems o){
        orderItemList.remove(o);
    }
    
    public Integer fetchOrderItem(int j)
    {
        int size = orderItemList.size();
       for(int i=j;i<=size;i++)
       {
           OrderItems items = (OrderItems) orderItemList.get(i);
           int s = items.getSalesPrice();
           return s;
       }
       return 0;
    }
    public Integer fetch()
    {
       int size = orderItemList.size();
       for(int i=0;i<=size;i++)
       {
           OrderItems items = (OrderItems) orderItemList.get(i);
           int s = items.getQuantity();
           return s;
       }
       return 0;
    }
    
    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Order.count = count;
    }

    public ArrayList<OrderItems> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItems> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Salesperson getSalesperson() {
        return salesperson;
    }

    public void setSalesperson(Salesperson salesperson) {
        this.salesperson = salesperson;
    }

}
