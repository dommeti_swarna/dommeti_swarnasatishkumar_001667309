/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author Swarna
 */
public class CustomerCatalog {
    
    private ArrayList<Customer> customerList;
    
    
    public CustomerCatalog()
    {
        
        customerList = new ArrayList<>();
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.customerList = customerList;
    }
    
    public Customer addCustomer()
    {
        Customer c = new Customer();
        customerList.add(c);
        return c;
    }
    
    public void removeCustomer(Customer c)
    {
        customerList.remove(c);
    }
    
    public Customer searchCustomer(int customerID)
    {
        for(Customer customer: customerList)
        {
            if(customer.getCustomerID()==customerID)
            {
                return customer;
            }
        }
        return null;
    }

    
    
}
