/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.KennelRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Pet.Pet;
import Business.PetOwner.PetOwner;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.PetOwnerKennelWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Swarna This class allows the Kennel Owner to process the Kennel
 * request sent by the Pet Owner.
 */
public class KennelRequestJPanel extends javax.swing.JPanel {

    /**
     * Creates new form KennelRequestJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Organization organization;
    private Enterprise enterprise;
    private EcoSystem business;
    private String ownerFound = null;
    private KennelWorkAreaJPanel work;
    private PetOwnerKennelWorkRequest reqFound = null;
    private PetOwnerKennelWorkRequest reqFound1 = null;

    public KennelRequestJPanel(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Enterprise enterprise, EcoSystem business, KennelWorkAreaJPanel work) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = userAccount;
        this.organization = organization;
        this.enterprise = enterprise;
        this.business = business;
        this.work = work;
        populateComboBox();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        kennelTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        confirmationTextField = new javax.swing.JTextField();
        submitButton = new javax.swing.JButton();
        petOwnerComboBox = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        processButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();

        kennelTable.setBackground(new java.awt.Color(204, 255, 255));
        kennelTable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        kennelTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Pet Name", "From Date", "To Date", "Status"
            }
        ));
        jScrollPane1.setViewportView(kennelTable);

        jLabel1.setText("Type \"Yes\" and click on Process Button to confirm");

        submitButton.setBackground(new java.awt.Color(204, 255, 255));
        submitButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        submitButton.setText("SUBMIT");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        petOwnerComboBox.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        petOwnerComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        petOwnerComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                petOwnerComboBoxActionPerformed(evt);
            }
        });

        jLabel2.setText("Pet Owner");

        processButton.setBackground(new java.awt.Color(204, 255, 255));
        processButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        processButton.setText("PROCESS");
        processButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processButtonActionPerformed(evt);
            }
        });

        backButton.setBackground(new java.awt.Color(204, 255, 255));
        backButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(52, 52, 52)
                                .addComponent(petOwnerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0))))
                    .addComponent(backButton))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(116, 116, 116)
                .addComponent(processButton)
                .addGap(138, 138, 138)
                .addComponent(submitButton)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(36, 36, 36)
                .addComponent(confirmationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(petOwnerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(confirmationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(processButton)
                    .addComponent(submitButton))
                .addGap(27, 27, 27)
                .addComponent(backButton))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void petOwnerComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_petOwnerComboBoxActionPerformed
        String name = String.valueOf(petOwnerComboBox.getSelectedItem());
        ownerFound = name;
        if (name != null) {
            populateTable(name);
        }
    }//GEN-LAST:event_petOwnerComboBoxActionPerformed

    /* This button allows the Kennel Owner to process the request only if the Kennel Owner confirms the
     appointment message with "Yes"
     */
    private void processButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processButtonActionPerformed
        String response = confirmationTextField.getText();

        int selectedRow = kennelTable.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table");
            return;
        }
        Pet pet = (Pet) kennelTable.getValueAt(selectedRow, 0);

        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
            if (request instanceof PetOwnerKennelWorkRequest) {
                PetOwnerKennelWorkRequest req = (PetOwnerKennelWorkRequest) request;
                if (req.getPet().equals(pet) && req.getStatus().equals("Sent")) {
                    if (response.equals("Yes")) {
                        req.setStatus("Confirmed");
                        reqFound1 = req;
                        JOptionPane.showMessageDialog(null, "Your request has been processed");
                        return;
                    } else {
                        JOptionPane.showMessageDialog(null, "Please type Yes to confirm the appointment");
                        return;
                    }
                }
            }
        }
    }//GEN-LAST:event_processButtonActionPerformed

    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitButtonActionPerformed
        populateTable(ownerFound);

    }//GEN-LAST:event_submitButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        work.fetch(reqFound1);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JTextField confirmationTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable kennelTable;
    private javax.swing.JComboBox petOwnerComboBox;
    private javax.swing.JButton processButton;
    private javax.swing.JButton submitButton;
    // End of variables declaration//GEN-END:variables

    public void populateTable(String owner) {
        DefaultTableModel dtm = (DefaultTableModel) kennelTable.getModel();
        dtm.setRowCount(0);

        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
            if (request instanceof PetOwnerKennelWorkRequest) {
                PetOwnerKennelWorkRequest req = (PetOwnerKennelWorkRequest) request;
                if (req.getPetOwner() == null) {
                    break;
                }
                if (req.getPetOwner().equals(owner)) {
                    Object row[] = new Object[4];
                    row[0] = ((PetOwnerKennelWorkRequest) request).getPet();
                    row[1] = ((PetOwnerKennelWorkRequest) request).getPetFromDate();
                    row[2] = ((PetOwnerKennelWorkRequest) request).getPetToDate();
                    row[3] = request.getStatus();
                    dtm.addRow(row);
                }
            }
        }

    }

    public void populateComboBox() {
        boolean exists = false;
        petOwnerComboBox.removeAllItems();
        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
            if (request instanceof PetOwnerKennelWorkRequest) {
                PetOwnerKennelWorkRequest req = (PetOwnerKennelWorkRequest) request;
                reqFound = req;
                for (int i = 0; i < petOwnerComboBox.getItemCount() && !exists; i++) {
                    if (req.getPetOwner().equals(petOwnerComboBox.getItemAt(i))) {
                        exists = true;
                    }
                }
                if (!exists) {

                    petOwnerComboBox.addItem(req.getPetOwner());
                }

            }
        }
    }

}
