/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.PetOwnerRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.KennelEnterprise;
import Business.Enterprise.VeterinaryCentersEnterprise;
import Business.Kennel.Kennel;
import Business.Network.Network;
import Business.Organization.KennelOrganization;
import Business.Organization.Organization;
import Business.Organization.VeterinaryDoctorOrganization;
import Business.Person.Person;
import Business.PetOwner.PetOwner;
import Business.UserAccount.UserAccount;
import Business.VeterinaryDoctor.VeterinaryDoctor;
import Business.WorkQueue.KennelWorkRequest;
import Business.WorkQueue.PetOwnerKennelWorkRequest;
import Business.WorkQueue.VeterinaryWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Swarna
 * This class helps to view all the appointments schedules by the Pet Owner
 */
public class ViewScheduledAppointmentsJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewScheduledAppointmentsJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Organization organization;
    private Enterprise enterprise;
    private EcoSystem business;

    public ViewScheduledAppointmentsJPanel(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Enterprise enterprise, EcoSystem business) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = userAccount;
        this.organization = organization;
        this.enterprise = enterprise;
        //this.petOwnerWorkRequest = petOwnerWorkRequest;
        this.business = business;
        populateComboBox();
        populateComboBoxKennel();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        veterinaryDoctorComboBox = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        viewVeterinaryDoctorTable = new javax.swing.JTable();
        ratingVetButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        kennelTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        kennelComboBox = new javax.swing.JComboBox();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable1);

        jLabel1.setText("Select a Veterinary Doctor");

        veterinaryDoctorComboBox.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        veterinaryDoctorComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        veterinaryDoctorComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                veterinaryDoctorComboBoxActionPerformed(evt);
            }
        });

        viewVeterinaryDoctorTable.setBackground(new java.awt.Color(204, 255, 255));
        viewVeterinaryDoctorTable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        viewVeterinaryDoctorTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Date of Appointment", "Status", "Message", "Pet Name"
            }
        ));
        jScrollPane1.setViewportView(viewVeterinaryDoctorTable);

        ratingVetButton.setBackground(new java.awt.Color(204, 255, 255));
        ratingVetButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ratingVetButton.setText("Rate the Veterinary Doctor");
        ratingVetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ratingVetButtonActionPerformed(evt);
            }
        });

        backButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        kennelTable.setBackground(new java.awt.Color(204, 255, 255));
        kennelTable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        kennelTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "From Date", "To Date", "Status", "Pet Name"
            }
        ));
        jScrollPane3.setViewportView(kennelTable);

        jLabel2.setText("Select a Kennel");

        kennelComboBox.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        kennelComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        kennelComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kennelComboBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backButton)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(49, 49, 49)
                                .addComponent(kennelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(49, 49, 49)
                                .addComponent(veterinaryDoctorComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ratingVetButton)
                .addGap(194, 194, 194))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(veterinaryDoctorComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(66, 66, 66)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ratingVetButton)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(kennelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(backButton))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void veterinaryDoctorComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_veterinaryDoctorComboBoxActionPerformed

        VeterinaryDoctor vet = (VeterinaryDoctor) veterinaryDoctorComboBox.getSelectedItem();
        if (vet != null) {
            populateTable(vet);
        }

    }//GEN-LAST:event_veterinaryDoctorComboBoxActionPerformed

    private void ratingVetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ratingVetButtonActionPerformed

        VeterinaryDoctor doc = (VeterinaryDoctor) veterinaryDoctorComboBox.getSelectedItem();

        VeterinaryDoctorRatingJPanel rating = new VeterinaryDoctorRatingJPanel(userProcessContainer, userAccount, organization, enterprise, business, doc);
        userProcessContainer.add("Veterinary Ratings", rating);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);

    }//GEN-LAST:event_ratingVetButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed

    private void kennelComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kennelComboBoxActionPerformed

        Kennel ken = (Kennel) kennelComboBox.getSelectedItem();
        if (ken != null) {
            populateTableKennel(ken);
        }

    }//GEN-LAST:event_kennelComboBoxActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JComboBox kennelComboBox;
    private javax.swing.JTable kennelTable;
    private javax.swing.JButton ratingVetButton;
    private javax.swing.JComboBox veterinaryDoctorComboBox;
    private javax.swing.JTable viewVeterinaryDoctorTable;
    // End of variables declaration//GEN-END:variables

    public void populateComboBox() {
        veterinaryDoctorComboBox.removeAllItems();
        for (Network network : business.getNetworkList()) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (enterprise instanceof VeterinaryCentersEnterprise) {
                    for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                        if (organization instanceof VeterinaryDoctorOrganization) {
                            for (Person person : organization.getPersonDirectory().getPersonList()) {
                                veterinaryDoctorComboBox.addItem(person);
                            }
                        }
                    }

                }
            }
        }
    }

    public void populateComboBoxKennel() {
        kennelComboBox.removeAllItems();
        for (Network network : business.getNetworkList()) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (enterprise instanceof KennelEnterprise) {
                    for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                        if (organization instanceof KennelOrganization) {
                            for (Person person : organization.getPersonDirectory().getPersonList()) {
                                kennelComboBox.addItem(person);
                            }
                        }
                    }

                }
            }
        }

    }

    public void populateTable(VeterinaryDoctor doc) {
        DefaultTableModel dtm = (DefaultTableModel) viewVeterinaryDoctorTable.getModel();
        dtm.setRowCount(0);

        for (WorkRequest wr : userAccount.getWorkQueue().getWorkRequestList()) {
            if (wr instanceof VeterinaryWorkRequest) {
                VeterinaryWorkRequest pet = (VeterinaryWorkRequest) wr;
                {

                    if (pet.getVetDoc().equals(doc)) {
                        Object row[] = new Object[4];
                        if (userAccount.getUserName().equals(wr.getSender().getUserName())) {
                            row[0] = ((VeterinaryWorkRequest) wr).getNextAppointmentDatePetOwner();
                        } else {
                            row[0] = ((VeterinaryWorkRequest) wr).getNextAppointmentDateVeterinary();
                        }
                        if (userAccount.getUserName().equals(wr.getSender().getUserName())) {
                            row[1] = ((VeterinaryWorkRequest) wr).getStatus();
                        } else {
                            row[1] = ((VeterinaryWorkRequest) wr).getFutureStatus();
                        }
                        if (userAccount.getUserName().equals(wr.getSender().getUserName())) {
                            if (wr.getStatus().equals("Completed")) {
                                row[2] = ((VeterinaryWorkRequest) wr).getTestResults();
                            } else if (wr.getStatus().equals("Sent")) {
                                row[2] = ((VeterinaryWorkRequest) wr).getMessage();
                            }
                        } else if (((VeterinaryWorkRequest) wr).getFutureStatus().equals("Pending")) {
                            row[2] = ((VeterinaryWorkRequest) wr).getFutureMessage();
                        }
                        row[3] = ((VeterinaryWorkRequest) wr).getPet().getPetName();
                        dtm.addRow(row);
                    }

                }

            }
        }

    }

    public void populateTableKennel(Kennel ken) {
        DefaultTableModel dtm = (DefaultTableModel) kennelTable.getModel();
        dtm.setRowCount(0);

        for (WorkRequest wr : userAccount.getWorkQueue().getWorkRequestList()) {
            if (wr instanceof PetOwnerKennelWorkRequest) {
                PetOwnerKennelWorkRequest req = (PetOwnerKennelWorkRequest) wr;
                {
                    if (req.getKennel().equals(ken)) {
                        Object row[] = new Object[4];
                        PetOwner own = (PetOwner) userAccount.getPerson();
                        if (own.getOwnerName().equals(req.getPetOwner())) {
                            row[0] = req.getPetFromDate();
                            row[1] = req.getPetToDate();
                            row[2] = req.getPet().getPetName();
                            row[3] = req.getStatus();
                            dtm.addRow(row);

                        }
                    }
                }
            }
        }
    }

}
