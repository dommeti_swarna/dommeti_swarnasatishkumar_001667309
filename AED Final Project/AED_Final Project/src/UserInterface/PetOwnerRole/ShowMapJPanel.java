/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.PetOwnerRole;

import Business.PetOwner.PetOwner;
import Business.Sensor.Circle;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Swarna
 */
public class ShowMapJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ShowMapJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private static PetOwner owner;
    private String longitude;
    private String latitude;
    private Image map;
    private int xMouse;
    private int yMouse;
    private int radius;
    private static JFrame foundFrame;
    private BufferedImage imageGraphics;

    public ShowMapJPanel(JPanel userProcessContainer, UserAccount userAccount, PetOwner owner, String latitude, String longitude) throws MalformedURLException, IOException {
        initComponents();
        this.userAccount = userAccount;
        this.userProcessContainer = userProcessContainer;
        this.owner = owner;
        this.longitude = longitude;
        this.latitude = latitude;
        display();
        populateComboBox();

    }
    
    /* This function displays the static map within a JFrame.
       It uses the mouse event handler to select the coordinates of a point.
    */
    public void display() throws MalformedURLException, IOException {

        String imageUrl = "http://maps.googleapis.com/maps/api/staticmap?&zoom=18&size=800x800&markers=" + latitude + "," + longitude + "&maptype=satellite&sensor=true";

        URL url = new URL(imageUrl);
        BufferedImage image = ImageIO.read(url);
        imageGraphics = image;
        JLabel label = new JLabel(new ImageIcon(image));
        JFrame f = new JFrame();
        foundFrame = f;
        f.getContentPane().add(label);
        f.pack();
        f.setLocation(200, 200);
        f.setVisible(true);
        f.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                xMouse = e.getX();
                xCoordinateTextField.setText(String.valueOf(xMouse));
                yMouse = e.getY();
                yCoordinateTextFiel.setText(String.valueOf(yMouse));

            }
        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sumbitButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        backButton = new javax.swing.JButton();
        milesComboBox = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        yCoordinateTextFiel = new javax.swing.JTextField();
        xCoordinateTextField = new javax.swing.JTextField();

        sumbitButton.setBackground(new java.awt.Color(204, 255, 255));
        sumbitButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sumbitButton.setText("SUBMIT");
        sumbitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sumbitButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("SET THE RADIUS OF THE CIRCULAR BOUNDARY WITHIN WHICH YOUR PET SHOULD RESIDE");

        backButton.setBackground(new java.awt.Color(204, 255, 255));
        backButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        milesComboBox.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        milesComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        milesComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                milesComboBoxActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CO-ORDINATES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel2.setText("X coordinate");

        jLabel3.setText("Y coordinate");

        yCoordinateTextFiel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        yCoordinateTextFiel.setEnabled(false);

        xCoordinateTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        xCoordinateTextField.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2))
                .addGap(73, 73, 73)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(yCoordinateTextFiel, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                    .addComponent(xCoordinateTextField))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(yCoordinateTextFiel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(xCoordinateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addComponent(jLabel3)))
                .addGap(32, 32, 32))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backButton)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(197, 197, 197)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(sumbitButton)
                            .addComponent(jLabel4)
                            .addComponent(milesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(milesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(jLabel4)
                .addGap(17, 17, 17)
                .addComponent(sumbitButton)
                .addGap(20, 20, 20)
                .addComponent(backButton))
        );
    }// </editor-fold>//GEN-END:initComponents

    //Functions draws a circle around the point selected using Graphics2D class
    private void sumbitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sumbitButtonActionPerformed

        owner.setxCoordinate(Integer.parseInt(xCoordinateTextField.getText()));
        owner.setyCoordinate(Integer.parseInt(yCoordinateTextFiel.getText()));
        radius = owner.getRadius();
        Graphics2D g2 = imageGraphics.createGraphics();
        Point p1 = new Point(xMouse, yMouse);
        xMouse = p1.x - radius;
        yMouse = p1.y - radius;
        g2.drawOval(xMouse, yMouse, 2 * radius, 2 * radius);
        foundFrame.setVisible(true);
        foundFrame.setExtendedState(foundFrame.getExtendedState() | JFrame.MAXIMIZED_BOTH);

    }//GEN-LAST:event_sumbitButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed

    //This function accepts the range of the radius selected by the user
    private void milesComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_milesComboBoxActionPerformed
        String selectedMile = String.valueOf(milesComboBox.getSelectedItem());
        if (selectedMile != null) {
            if (selectedMile == "1 mile") {
                owner.setRadius(100);
            } else if (selectedMile == "2 miles") {
                owner.setRadius(200);
            }
        }
    }//GEN-LAST:event_milesComboBoxActionPerformed

    public void populateComboBox() {
        milesComboBox.removeAllItems();
        milesComboBox.addItem("1 mile");
        milesComboBox.addItem("2 miles");
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JComboBox milesComboBox;
    private javax.swing.JButton sumbitButton;
    private javax.swing.JTextField xCoordinateTextField;
    private javax.swing.JTextField yCoordinateTextFiel;
    // End of variables declaration//GEN-END:variables
}
