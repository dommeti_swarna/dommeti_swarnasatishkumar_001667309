/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.PetOwnerRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Organization.VeterinaryDoctorOrganization;
import Business.Person.Person;
import Business.Pet.Pet;
import Business.Pet.VitalSign;
import Business.Pet.VitalSignDirectory;
import Business.PetOwner.PetOwner;
import Business.Sensor.Sensor;
import Business.UserAccount.UserAccount;
import Business.VeterinaryDoctor.VeterinaryDoctor;
import Business.WorkQueue.VitalSignVetWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Swarna
 */
public class SensorJPanel extends javax.swing.JPanel {

    /**
     * Creates new form SensorJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Organization organization;
    private Enterprise enterprise;
    private Pet petFound = null;
    private EcoSystem business;
    private VeterinaryDoctor vetFound = null;
    ArrayList<VitalSignVetWorkRequest> requestList = new ArrayList<>();

    public SensorJPanel(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Enterprise enterprise, EcoSystem business) {
        initComponents();
        this.userAccount = userAccount;
        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.enterprise = enterprise;
        this.business = business;
        populateComboBox();
        Pet petFound = (Pet) petNameComboBox.getSelectedItem();
        if (!(petFound == null)) {
            populateTableDog(petFound);
        }
        populateVetComboBox();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        vitalSignsTable = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        petNameComboBox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        petAgeTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        petTypeTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        tagIDTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        messageFromVet = new javax.swing.JTextField();
        viewVetMessage = new javax.swing.JButton();
        generateButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        vetComboBox = new javax.swing.JComboBox();

        jLabel1.setText("jLabel1");

        vitalSignsTable.setBackground(new java.awt.Color(204, 255, 255));
        vitalSignsTable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        vitalSignsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Heart Rate", "Temperature", "Respiration", "Status", "Timestamp"
            }
        ));
        jScrollPane1.setViewportView(vitalSignsTable);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PET DETAILS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel2.setText("Pet Name");

        petNameComboBox.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        petNameComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        petNameComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                petNameComboBoxActionPerformed(evt);
            }
        });

        jLabel3.setText("Pet Age");

        petAgeTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        petAgeTextField.setEnabled(false);

        jLabel4.setText("Pet Type");

        petTypeTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        petTypeTextField.setEnabled(false);

        jLabel5.setText("Tag ID");

        tagIDTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tagIDTextField.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(45, 45, 45)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(petNameComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(petAgeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(petTypeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tagIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(petNameComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(petTypeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(petAgeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(tagIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel6.setText("Message from Vet");

        viewVetMessage.setBackground(new java.awt.Color(204, 255, 255));
        viewVetMessage.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        viewVetMessage.setText("VIEW VET MESSAGE");
        viewVetMessage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewVetMessageActionPerformed(evt);
            }
        });

        generateButton.setBackground(new java.awt.Color(204, 255, 255));
        generateButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generateButton.setText("GENERATE");
        generateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateButtonActionPerformed(evt);
            }
        });

        backButton.setBackground(new java.awt.Color(204, 255, 255));
        backButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        jLabel7.setText("Veterinary Doctor");

        vetComboBox.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        vetComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        vetComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vetComboBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(0, 0, 0))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)
                        .addContainerGap())))
            .addComponent(messageFromVet, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backButton)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(67, 67, 67)
                                .addComponent(vetComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(viewVetMessage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(generateButton)
                .addGap(77, 77, 77))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(vetComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(messageFromVet, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(viewVetMessage)
                    .addComponent(generateButton))
                .addGap(73, 73, 73)
                .addComponent(backButton))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void petNameComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_petNameComboBoxActionPerformed
        Pet pet = (Pet) petNameComboBox.getSelectedItem();
        petFound = pet;
        if (pet != null) {
            populateFields(pet);
            populateTableDog(pet);
        }
    }//GEN-LAST:event_petNameComboBoxActionPerformed

    private void viewVetMessageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewVetMessageActionPerformed

        populateField();
    }//GEN-LAST:event_viewVetMessageActionPerformed

    /*This function calls the Sensor class which generates random values of Vital Signs
      These values generated are added to the VitalSignVetWork Request so that the Veterinary Doctor is
      able to examine the vital signs.
    */
    private void generateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateButtonActionPerformed
        Pet petFound = (Pet) petNameComboBox.getSelectedItem();
        Sensor sen = new Sensor();

        PetOwner owner = (PetOwner) userAccount.getPerson();
        for (Pet pet : owner.getPetDirectory().getPetList()) {
            if (petFound.getPetName().equals(pet.getPetName())) {
                if (pet.getVitalSignList() == null) {
                    VitalSignDirectory vsDirect = new VitalSignDirectory();
                    pet.setVitalSignList(vsDirect);
                    VitalSign vs = vsDirect.addVital();

                    int heartRate = sen.calHeartRate();
                    int respiratoryRate = sen.calRespirationRate();
                    float temperature = (float) sen.calTemeperature();
                    Date date = new Date();

                    vs.setHeartRate(heartRate);
                    vs.setRespiratoryRate(respiratoryRate);
                    vs.setTemperature(heartRate);
                    vs.setDate(String.valueOf(date));

                    vs.setStatus(getStatus(heartRate, temperature, respiratoryRate));

                    if ((vs.getStatus().equals("Abnormal - Temperature")) || (vs.getStatus().equals("Abnormal - Respiratory Rate")) || (vs.getStatus().equals("Abnormal - Heart Rate")) || (vs.getStatus().equals("Abnormal - all Vital Signs are failing"))) {
                        VitalSignVetWorkRequest req = new VitalSignVetWorkRequest();
                        req.setPet(petFound);
                        req.setHeartRate(vs.getHeartRate());
                        req.setRespiratoryRate(vs.getRespiratoryRate());
                        req.setTemperature((int) vs.getTemperature());
                        req.setPetOwner(userAccount);
                        req.setSensorStatus(vs.getStatus());
                        req.setVeterinaryDoctor(vetFound);
                        req.setDate(String.valueOf(date));
                        req.setMessage("Sending message");
                           // populateComment();

                        Organization org = null;
                        Enterprise ent = null;
                        for (Network network : business.getNetworkList()) {
                            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                                ent = enterprise;
                                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                                    if (organization instanceof VeterinaryDoctorOrganization) {
                                        org = organization;
                                        break;
                                    }
                                }

                            }
                        }
                        if (org != null) {
                            org.getWorkQueue().getWorkRequestList().add(req);
                            userAccount.getWorkQueue().getWorkRequestList().add(req);
                        }

                    }

                } else {
                    VitalSignDirectory vsDirect = pet.getVitalSignList();
                    VitalSign vs = vsDirect.addVital();

                    int heartRate = sen.calHeartRate();
                    int respiratoryRate = sen.calRespirationRate();
                    float temperature = (float) sen.calTemeperature();
                    Date date = new Date();

                    vs.setHeartRate(heartRate);
                    vs.setRespiratoryRate(respiratoryRate);
                    vs.setTemperature(heartRate);
                    vs.setStatus(getStatus(heartRate, temperature, respiratoryRate));
                    vs.setDate(String.valueOf(date));

                    if ((vs.getStatus().equals("Abnormal - Temperature")) || (vs.getStatus().equals("Abnormal - Respiratory Rate")) || (vs.getStatus().equals("Abnormal - Heart Rate")) || (vs.getStatus().equals("Abnormal - all Vital Signs are failing"))) {
                        VitalSignVetWorkRequest req = new VitalSignVetWorkRequest();
                        req.setPet(petFound);
                        req.setHeartRate(vs.getHeartRate());
                        req.setRespiratoryRate(vs.getRespiratoryRate());
                        req.setTemperature((int) vs.getTemperature());
                        req.setPetOwner(userAccount);
                        req.setSensorStatus(vs.getStatus());
                        req.setVeterinaryDoctor(vetFound);
                        req.setDate(vs.getDate());
                        req.setMessage("Sending message");
                           // populateComment();

                        Organization org = null;
                        Enterprise ent = null;
                        for (Network network : business.getNetworkList()) {
                            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                                ent = enterprise;
                                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                                    if (organization instanceof VeterinaryDoctorOrganization) {
                                        org = organization;
                                        break;
                                    }
                                }

                            }
                        }
                        if (org != null) {
                            org.getWorkQueue().getWorkRequestList().add(req);
                            userAccount.getWorkQueue().getWorkRequestList().add(req);
                        }

                    }
                }
            }
        }
        populateTableDog(petFound);


    }//GEN-LAST:event_generateButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed

    private void vetComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vetComboBoxActionPerformed
        VeterinaryDoctor vet = (VeterinaryDoctor) vetComboBox.getSelectedItem();
        if (vet != null) {
            vetFound = vet;
        }
    }//GEN-LAST:event_vetComboBoxActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JButton generateButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField messageFromVet;
    private javax.swing.JTextField petAgeTextField;
    private javax.swing.JComboBox petNameComboBox;
    private javax.swing.JTextField petTypeTextField;
    private javax.swing.JTextField tagIDTextField;
    private javax.swing.JComboBox vetComboBox;
    private javax.swing.JButton viewVetMessage;
    private javax.swing.JTable vitalSignsTable;
    // End of variables declaration//GEN-END:variables

    public void populateComboBox() {
        petNameComboBox.removeAllItems();
        PetOwner owner = (PetOwner) userAccount.getPerson();
        for (Pet pet : owner.getPetDirectory().getPetList()) {
            petNameComboBox.addItem(pet);
        }

    }

    public void populateFields(Pet pet) {
        petAgeTextField.setText(String.valueOf(pet.getPetAge()));
        petTypeTextField.setText(pet.getPetType());
        tagIDTextField.setText(String.valueOf(pet.getPetTagID()));

    }

    public void populateTableDog(Pet petFound) {
        DefaultTableModel dtm = (DefaultTableModel) vitalSignsTable.getModel();
        dtm.setRowCount(0);

        vitalSignsTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        vitalSignsTable.getColumnModel().getColumn(1).setPreferredWidth(100);
        vitalSignsTable.getColumnModel().getColumn(2).setPreferredWidth(100);
        vitalSignsTable.getColumnModel().getColumn(3).setPreferredWidth(200);
        vitalSignsTable.getColumnModel().getColumn(4).setPreferredWidth(350);

        if (petFound != null) {
            if (petFound.getVitalSignList() != null) {
                for (VitalSign vitalSign : petFound.getVitalSignList().getVitalSignList()) {
                    Object row[] = new Object[5];
                    row[0] = vitalSign.getHeartRate();
                    row[1] = vitalSign.getTemperature();
                    row[2] = vitalSign.getRespiratoryRate();
                    row[3] = vitalSign.getStatus();
                    row[4] = vitalSign.getDate();
                    dtm.addRow(row);
                }
            }
        }
    }

    /*This function compares the different ranges of Vital Sign with the normal range values 
      and displays Normal if it is within the range and Abnormal if it is out of the range
    */
    public String getStatus(int heartRate, float temperature, int respiratoryRate) {
        if ((heartRate < 80)) {
            String status = "Abnormal - Heart Rate";
            return status;
        } else if (temperature < 100.5) {
            String status = "Abnormal - Temperature";
            return status;
        } else if (respiratoryRate < 24) {
            String status = "Abnormal - Respiration Rate";
            return status;
        } else if (heartRate < 80 && temperature < 100.5 && respiratoryRate < 24) {
            String status = "Abnormal - all Vital Signs are failing";
            return status;
        } else if (heartRate > 150) {
            String status = "Abnormal - Heart Rate";
            return status;
        } else if (temperature > 102.5) {
            String status = "Abnormal - Temperature";
            return status;
        } else if (respiratoryRate > 42) {
            String status = "Abnormal - Respiration Rate";
            return status;
        } else if (heartRate > 150 && temperature > 102.5 && respiratoryRate > 42) {
            String status = "Abnormal - all Vital Signs are failing";
            return status;
        } else {
            String status = "Normal";
            return status;
        }

    }

    public void populateField() {
        for (WorkRequest request : userAccount.getWorkQueue().getWorkRequestList()) {
            if (request instanceof VitalSignVetWorkRequest) {
                VitalSignVetWorkRequest req = (VitalSignVetWorkRequest) request;
                requestList.add(req);
                Collections.sort(requestList);
            }
        }

        for (int i = 0; i < requestList.size(); i++) {
            VitalSignVetWorkRequest req = (VitalSignVetWorkRequest) requestList.get(i);
            if (req.getPet().equals(petFound) && req.getVeterinaryDoctor().equals(vetFound)) {
                messageFromVet.setText(req.getStatus());
                break;
            }
        }
    }

    public void populateVetComboBox() {
        vetComboBox.removeAllItems();
        for (Network network : business.getNetworkList()) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    if (organization instanceof VeterinaryDoctorOrganization) {
                        for (Person person : organization.getPersonDirectory().getPersonList()) {
                            VeterinaryDoctor vet = (VeterinaryDoctor) person;
                            vetComboBox.addItem(vet);
                        }

                    }
                }
            }
        }
    }
}
