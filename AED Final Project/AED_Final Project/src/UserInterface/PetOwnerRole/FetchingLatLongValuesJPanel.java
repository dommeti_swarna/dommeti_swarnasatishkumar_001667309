/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.PetOwnerRole;

import Business.PetOwner.PetOwner;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;

/**
 *
 * @author Swarna
 * This class fetches the latitude and longitude values of a location using Google Maps API
 * It displays a static map based on the location.
 * Using mouse event handler we can select a point on the map, which acts as a center to draw a secured circle
 * thus helping to track the location of the pet.
 */
public class FetchingLatLongValuesJPanel extends javax.swing.JPanel {

    /**
     * Creates new form FetchingLatLongValuesJPanel
     */
    private String postcode;
    private String latLongs[];
    private String URL;
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private String latitude;
    private String longitude;
    private static PetOwner ownerFound = null;

    public FetchingLatLongValuesJPanel(JPanel userProcessContainer, UserAccount userAccount) {
        initComponents();
        this.userAccount = userAccount;
        this.userProcessContainer = userProcessContainer;
        System.setProperty("java.net.useSystemProxies", "true");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        PetOwner owner = (PetOwner) userAccount.getPerson();
        ownerFound = owner;
        String city = owner.getOwnerCity();
        String street = owner.getOwnerStreetName();
        locationTextField.setText(street + city);
        postcode = owner.getOwnerCity();

    }

    /*This function converts the api obtained using postcode into the URL which helps to display static
    map */
    public static String[] getLatLongPositions(String address) throws Exception {
        int responseCode = 0;
        String api = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + URLEncoder.encode(address, "UTF-8") + "&sensor=true";
        ownerFound.setURL(api);
        URL url = new URL(api);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.connect();
        responseCode = httpConnection.getResponseCode();
        if (responseCode == 200) {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();;
            Document document = builder.parse(httpConnection.getInputStream());
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("/GeocodeResponse/status");
            String status = (String) expr.evaluate(document, XPathConstants.STRING);
            if (status.equals("OK")) {
                expr = xpath.compile("//geometry/location/lat");
                String latitude = (String) expr.evaluate(document, XPathConstants.STRING);
                expr = xpath.compile("//geometry/location/lng");
                String longitude = (String) expr.evaluate(document, XPathConstants.STRING);
                return new String[]{latitude, longitude};
            } else {
                throw new Exception("Error from the API - response status: " + status);
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        locationTextField = new javax.swing.JTextField();
        showButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        latitudeTextField = new javax.swing.JTextField();
        longitudeTextField = new javax.swing.JTextField();
        showMapsButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();

        jLabel1.setText("Location");

        locationTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locationTextField.setEnabled(false);

        showButton.setBackground(new java.awt.Color(204, 255, 255));
        showButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        showButton.setText("SHOW LATITUDE AND LOGITUDE OF THE LOCATION");
        showButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Latitude");

        jLabel3.setText("Longitude");

        latitudeTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        latitudeTextField.setEnabled(false);

        longitudeTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        longitudeTextField.setEnabled(false);

        showMapsButton.setBackground(new java.awt.Color(204, 255, 255));
        showMapsButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        showMapsButton.setText("SHOW MAP");
        showMapsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showMapsButtonActionPerformed(evt);
            }
        });

        backButton.setBackground(new java.awt.Color(204, 255, 255));
        backButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backButton)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(showButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(locationTextField)
                            .addComponent(latitudeTextField)
                            .addComponent(longitudeTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(showMapsButton)))))
                .addGap(42, 42, 42))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(locationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addComponent(showButton)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(latitudeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(longitudeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(showMapsButton)
                .addGap(19, 19, 19)
                .addComponent(backButton))
        );
    }// </editor-fold>//GEN-END:initComponents
    /* This function sets the obtained latitude and longitude to the text fields for display*/
    private void showButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showButtonActionPerformed

        try {
            if (postcode != null) {
                latLongs = getLatLongPositions(postcode);
                latitudeTextField.setText(latLongs[0]);
                longitudeTextField.setText(latLongs[1]);

                latitude = latLongs[0];
                longitude = latLongs[1];
            } else {
                JOptionPane.showMessageDialog(null, "Please create Pet Owner's profile");
                return;
            }
        } catch (Exception ex) {
            Logger.getLogger(FetchingLatLongValuesJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_showButtonActionPerformed

    /* This function opens a static map on a new JPanel*/
    private void showMapsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showMapsButtonActionPerformed

        if (latitude != null && longitude != null) {
            ShowMapJPanel maps = null;
            try {
                maps = new ShowMapJPanel(userProcessContainer, userAccount, ownerFound, latitude, longitude);
                userProcessContainer.add("Showing google maps", maps);
                CardLayout layout = (CardLayout) userProcessContainer.getLayout();
                layout.next(userProcessContainer);
            } catch (MalformedURLException ex) {
                Logger.getLogger(FetchingLatLongValuesJPanel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(FetchingLatLongValuesJPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "You need to fetch the latitude and logitude to display the map");
            return;
        }


    }//GEN-LAST:event_showMapsButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField latitudeTextField;
    private javax.swing.JTextField locationTextField;
    private javax.swing.JTextField longitudeTextField;
    private javax.swing.JButton showButton;
    private javax.swing.JButton showMapsButton;
    // End of variables declaration//GEN-END:variables
}
