/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.VeterinaryDoctorRole;

import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Organization.PetOwnerOrganization;
import Business.Person.Person;
import Business.Pet.Pet;
import Business.PetOwner.PetOwner;
import Business.UserAccount.UserAccount;
import Business.VeterinaryDoctor.VeterinaryDoctor;
import Business.WorkQueue.KennelWorkRequest;
import Business.WorkQueue.VeterinaryWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Swarna
 */
public class AppointmentSchedulingJPanel extends javax.swing.JPanel {

    /**
     * Creates new form AppointmentSchedulingJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Organization organization;
    private Enterprise enterprise;
    //private VeterinaryWorkRequest veterinaryWorkRequest;

    public AppointmentSchedulingJPanel(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = userAccount;
        this.organization = organization;
        this.enterprise = enterprise;
        populateComboBox();

    }

    public void populateComboBox() {
        boolean exists = false;
        boolean exists1 = false;
        Pet petFound = null;
        petNameComboBox.removeAllItems();
        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
            if (request instanceof VeterinaryWorkRequest) {
                VeterinaryWorkRequest vet = (VeterinaryWorkRequest) request;
                if (vet.getVetDoc().equals(userAccount.getPerson())) {
                    Pet name = vet.getPet();
                    for (int i = 0; i < petNameComboBox.getItemCount() && !exists; i++) {
                        if ((vet.getPet()).equals(petNameComboBox.getItemAt(i))) {
                            exists = true;
                        }
                    }
                    if (!exists) {
                        petNameComboBox.addItem(vet.getPet());
                        petFound = vet.getPet();
                    }
                }
            } else if (request instanceof KennelWorkRequest) {
                KennelWorkRequest vet = (KennelWorkRequest) request;
                if (vet.getVetPerson().equals(userAccount.getPerson())) {
                    Pet name = vet.getPet();
                    for (int i = 0; i < petNameComboBox.getItemCount() && !exists1; i++) {
                        if ((vet.getPet()).equals(petNameComboBox.getItemAt(i))) {
                            exists1 = true;
                        }
                    }
                    if (!exists1) {
                        petNameComboBox.addItem(vet.getPet());
                        petFound = vet.getPet();
                    }
                }

            }

        }

    }

    //Function populates table based on the type of request sent and approved.
    public void populateTable(Pet pet) {
        DefaultTableModel dtm = (DefaultTableModel) futureAppointmentTable.getModel();
        dtm.setRowCount(0);

        for (WorkRequest wr : organization.getWorkQueue().getWorkRequestList()) {
            if (wr instanceof VeterinaryWorkRequest) {
                VeterinaryWorkRequest own = (VeterinaryWorkRequest) wr;
                if (own.getPet().equals(pet)) {

                    Object row[] = new Object[4];
                    if (userAccount.equals(own.getSender())) {
                        row[0] = ((VeterinaryWorkRequest) wr).getNextAppointmentDateVeterinary();
                        row[1] = ((VeterinaryWorkRequest) wr).getFutureStatus();
                        row[2] = ((VeterinaryWorkRequest) wr).getSender().getUserName();
                        row[3] = ((VeterinaryWorkRequest) wr).getFutureMessage();
                        dtm.addRow(row);
                    } else {
                        row[0] = ((VeterinaryWorkRequest) wr).getNextAppointmentDatePetOwner();
                        row[1] = ((VeterinaryWorkRequest) wr).getStatus();
                        row[2] = ((VeterinaryWorkRequest) wr).getSender().getUserName();
                        row[3] = ((VeterinaryWorkRequest) wr).getMessage();
                        dtm.addRow(row);
                    }
                }

            } else if (wr instanceof KennelWorkRequest) {
                KennelWorkRequest own = (KennelWorkRequest) wr;

                if (own.getPet().equals(pet)) {

                    Object row[] = new Object[4];
                    if (userAccount.equals(own.getSender())) {
                        row[0] = ((KennelWorkRequest) wr).getAppointmentDate();
                        row[1] = ((KennelWorkRequest) wr).getFutureStatus();
                        row[2] = ((KennelWorkRequest) wr).getSender().getUserName();
                        row[3] = ((KennelWorkRequest) wr).getFutureMessage();
                        dtm.addRow(row);
                    } else if (own.getSender().getEnterprise().getType().getValue().equals("Pet Care System")) {
                        row[0] = ((KennelWorkRequest) wr).getAppointmentDate();
                        row[1] = ((KennelWorkRequest) wr).getStatus();
                        row[2] = ((KennelWorkRequest) wr).getSender().getUserName();
                        row[3] = ((KennelWorkRequest) wr).getMessage();
                        dtm.addRow(row);
                    }
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        futureAppointmentTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        petNameComboBox = new javax.swing.JComboBox();
        backButton = new javax.swing.JButton();

        futureAppointmentTable.setBackground(new java.awt.Color(204, 255, 255));
        futureAppointmentTable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        futureAppointmentTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Date of Appointment", "Status", "Booked By", "Message"
            }
        ));
        jScrollPane1.setViewportView(futureAppointmentTable);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Future Appointment of Pet");

        jLabel3.setText("Select Pet Name");

        petNameComboBox.setBackground(new java.awt.Color(204, 255, 255));
        petNameComboBox.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        petNameComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        petNameComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                petNameComboBoxActionPerformed(evt);
            }
        });

        backButton.setBackground(new java.awt.Color(204, 255, 255));
        backButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 615, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(37, 37, 37)
                        .addComponent(petNameComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(backButton)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(petNameComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(backButton))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void petNameComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_petNameComboBoxActionPerformed
        Pet pet = (Pet) petNameComboBox.getSelectedItem();
        if (pet != null) {
            populateTable(pet);
        }

    }//GEN-LAST:event_petNameComboBoxActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JTable futureAppointmentTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox petNameComboBox;
    // End of variables declaration//GEN-END:variables
}
