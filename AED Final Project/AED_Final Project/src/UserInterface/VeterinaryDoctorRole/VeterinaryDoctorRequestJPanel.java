/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.VeterinaryDoctorRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.PetCareEnterprise;
import Business.Enterprise.VeterinaryCentersEnterprise;
import Business.Kennel.Kennel;
import Business.Network.Network;
import Business.Organization.KennelOrganization;
import Business.Organization.Organization;
import Business.Organization.PetOwnerOrganization;
import Business.Organization.VeterinaryDoctorOrganization;
import Business.Pet.Pet;
import Business.PetOwner.PetOwner;
import Business.UserAccount.UserAccount;
import Business.Validations.Validation;
import Business.VeterinaryDoctor.VeterinaryDoctor;
import Business.WorkQueue.KennelWorkRequest;
import Business.WorkQueue.VeterinaryWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author raunak
 */
public class VeterinaryDoctorRequestJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Enterprise enterprise;
    private Organization organization;
    private VeterinaryWorkRequest request;
    private EcoSystem business;
    private Pet pet = null;
    private WorkRequest requestFound = null;
    VeterinaryWorkRequest vetRequest = null;
    KennelWorkRequest kenRequest = null;
    WorkRequest reqFound1 = null;
    String day;

    /**
     * Creates new form LabAssistantWorkAreaJPanel
     */
    public VeterinaryDoctorRequestJPanel(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, VeterinaryWorkRequest request, EcoSystem business) {
        initComponents();

        this.userProcessContainer = userProcessContainer;
        this.userAccount = account;
        this.enterprise = enterprise;
        this.organization = (VeterinaryDoctorOrganization) organization;
        this.request = request;
        this.business = business;

        populateTable();
    }

    //Populates the table based on the kind of request sent or approved.
    public void populateTable() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();

        model.setRowCount(0);

        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
            if (request instanceof VeterinaryWorkRequest) {

                VeterinaryWorkRequest vet = (VeterinaryWorkRequest) request;
                if (vet.getVetDoc().equals(userAccount.getPerson())) {
                    if (!(vet.getSender().getUserName().equals(userAccount.getUserName()))) {
                        Object[] row = new Object[7];
                        row[0] = request;
                        row[1] = request.getSender().getPerson().getName();
                        row[2] = request.getReceiver() == null ? null : request.getReceiver().getPerson().getName();
                        row[3] = ((VeterinaryWorkRequest) request).getNextAppointmentDatePetOwner();
                        row[4] = ((VeterinaryWorkRequest) request).getDayOfAppointment();
                        row[5] = request.getStatus();
                        row[6] = ((VeterinaryWorkRequest) request).getPet();
                        model.addRow(row);
                    } else if (vet.getSender().getPerson().equals(userAccount.getPerson())) {
                        Object[] row = new Object[7];
                        row[0] = ((VeterinaryWorkRequest) request);
                        row[1] = request.getSender().getPerson().getName();
                        row[2] = request.getReceiver() == null ? null : request.getReceiver().getPerson().getName();
                        row[3] = ((VeterinaryWorkRequest) request).getNextAppointmentDateVeterinary();
                        row[4] = ((VeterinaryWorkRequest) request).getDayOfAppointment();
                        row[5] = ((VeterinaryWorkRequest) request).getFutureStatus();
                        row[6] = ((VeterinaryWorkRequest) request).getPet();

                        model.addRow(row);
                    }
                }

            } else if (request instanceof KennelWorkRequest) {
                KennelWorkRequest kennel = (KennelWorkRequest) request;
                if (kennel.getVetPerson().equals(userAccount.getPerson())) {
                    if (!(kennel.getSender().getUserName().equals(userAccount.getUserName()))) {
                        Object[] row = new Object[7];
                        row[0] = request;
                        row[1] = request.getSender().getPerson().getName();
                        row[2] = request.getReceiver() == null ? null : request.getReceiver().getPerson().getName();
                        row[3] = ((KennelWorkRequest) request).getAppointmentDate();
                        row[4] = ((KennelWorkRequest) request).getAppointmentDay();
                        row[5] = request.getStatus();
                        row[6] = ((KennelWorkRequest) request).getPet();

                        model.addRow(row);
                    } else if (kennel.getSender().getPerson().equals(userAccount.getPerson())) {
                        Object[] row = new Object[7];
                        row[0] = (KennelWorkRequest) request;
                        row[1] = request.getSender().getPerson().getName();
                        row[2] = request.getReceiver() == null ? null : request.getReceiver().getPerson().getName();
                        row[3] = ((KennelWorkRequest) request).getAppointmentDate();
                        row[4] = ((KennelWorkRequest) request).getAppointmentDay();
                        row[5] = request.getStatus();
                        row[6] = ((KennelWorkRequest) request).getPet();

                        model.addRow(row);

                    }
                }
            }

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        assignJButton = new javax.swing.JButton();
        processJButton = new javax.swing.JButton();
        refreshJButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        bookFutureButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        reasonTextField = new javax.swing.JTextField();
        viewFeedbackButton = new javax.swing.JButton();
        jDateChooserAppointment = new com.toedter.calendar.JDateChooser();
        backButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();

        workRequestJTable.setBackground(new java.awt.Color(204, 255, 255));
        workRequestJTable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Message", "Sender", "Receiver", "Date of Appointment", "Day of Appointment", "Status", "Pet Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(workRequestJTable);
        if (workRequestJTable.getColumnModel().getColumnCount() > 0) {
            workRequestJTable.getColumnModel().getColumn(0).setResizable(false);
            workRequestJTable.getColumnModel().getColumn(1).setResizable(false);
            workRequestJTable.getColumnModel().getColumn(2).setResizable(false);
            workRequestJTable.getColumnModel().getColumn(3).setResizable(false);
        }

        assignJButton.setBackground(new java.awt.Color(204, 255, 255));
        assignJButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        assignJButton.setText("ASSIGN IT TO ME");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });

        processJButton.setBackground(new java.awt.Color(204, 255, 255));
        processJButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        processJButton.setText("PROCESS");
        processJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processJButtonActionPerformed(evt);
            }
        });

        refreshJButton.setBackground(new java.awt.Color(204, 255, 255));
        refreshJButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        refreshJButton.setText("Refresh");
        refreshJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshJButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Next Appointment Date");

        bookFutureButton.setBackground(new java.awt.Color(204, 255, 255));
        bookFutureButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bookFutureButton.setText("BOOK FUTURE APPOINMENT DATE");
        bookFutureButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bookFutureButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Reason");

        viewFeedbackButton.setBackground(new java.awt.Color(204, 255, 255));
        viewFeedbackButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        viewFeedbackButton.setText("VIEW FEEDBACK");
        viewFeedbackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewFeedbackButtonActionPerformed(evt);
            }
        });

        jDateChooserAppointment.setDateFormatString("yyyy MMM d");

        backButton.setBackground(new java.awt.Color(204, 255, 255));
        backButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        deleteButton.setBackground(new java.awt.Color(204, 255, 255));
        deleteButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        deleteButton.setText("DELETE REQUEST");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(67, 67, 67)
                        .addComponent(assignJButton)
                        .addGap(68, 68, 68)
                        .addComponent(deleteButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(processJButton)
                        .addGap(119, 119, 119)
                        .addComponent(viewFeedbackButton))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(jDateChooserAppointment, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(172, 172, 172)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(reasonTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(59, 59, 59))
            .addComponent(backButton)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(refreshJButton)
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(bookFutureButton)
                        .addGap(226, 226, 226))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(refreshJButton)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(assignJButton)
                    .addComponent(processJButton)
                    .addComponent(viewFeedbackButton)
                    .addComponent(deleteButton))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(reasonTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1))
                    .addComponent(jDateChooserAppointment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bookFutureButton)
                .addGap(35, 35, 35)
                .addComponent(backButton))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }
        WorkRequest request = (WorkRequest) workRequestJTable.getValueAt(selectedRow, 0);

        if (request.getStatus().equals("Request deleted")) {
            JOptionPane.showMessageDialog(null, "You cannot process the deleted request");
            return;
        } else {
            if (request instanceof VeterinaryWorkRequest) {
                requestFound = (VeterinaryWorkRequest) request;
                requestFound.setReceiver(userAccount);
                requestFound.setStatus("Pending");
            } else {
                requestFound = (KennelWorkRequest) request;
                requestFound.setReceiver(userAccount);
                requestFound.setStatus("Pending");
            }

            populateTable();
        }
    }//GEN-LAST:event_assignJButtonActionPerformed

    private void processJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processJButtonActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row whose has its request being assigned");
            return;
        }

        if (requestFound != null) {
            if (requestFound instanceof VeterinaryWorkRequest) {
                VeterinaryWorkRequest request = (VeterinaryWorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
                vetRequest = request;
                vetRequest.setStatus("Processing");
            } else if (requestFound instanceof KennelWorkRequest) {
                KennelWorkRequest request = (KennelWorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
                kenRequest = request;
                kenRequest.setStatus("Processing");
            }
            ProcessWorkRequestJPanel processWorkRequestJPanel = new ProcessWorkRequestJPanel(userProcessContainer, userAccount, organization, enterprise, vetRequest, kenRequest);
            userProcessContainer.add("processWorkRequestJPanel", processWorkRequestJPanel);
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            layout.next(userProcessContainer);
        } else {
            JOptionPane.showMessageDialog(null, "Please assign an receiver to the request");
        }
    }//GEN-LAST:event_processJButtonActionPerformed

    private void refreshJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshJButtonActionPerformed
        populateTable();
    }//GEN-LAST:event_refreshJButtonActionPerformed

    //Allows the Veterinary Doctor to book a future appointment for a pet.
    private void bookFutureButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bookFutureButtonActionPerformed

        VeterinaryDoctor vet = null;
        VeterinaryWorkRequest vetRequestNew = null;
        KennelWorkRequest kenRequestNew = null;

        int count = 1;
        int selectedRow = workRequestJTable.getSelectedRow();
        if (selectedRow < 0) {
            return;
        }
        WorkRequest requestSelected = (WorkRequest) workRequestJTable.getValueAt(selectedRow, 0);

        count = validateFields();

        if (count == 0) {

            Date datchooser = jDateChooserAppointment.getDate();
            String dateAppointment = String.valueOf(datchooser);
            String dayApp = String.valueOf(jDateChooserAppointment.getDate().getDay());

            if (dayApp.equals("1")) {
                day = "Sunday";
            } else if (dayApp.equals("2")) {
                day = "Monday";
            } else if (dayApp.equals("3")) {
                day = "Tuesday";
            } else if (dayApp.equals("3")) {
                day = "Wednesday";
            } else if (dayApp.equals("4")) {
                day = "Thursday";
            } else if (dayApp.equals("5")) {
                day = "Friday";
            } else if (dayApp.equals("6")) {
                day = "Saturday";
            }

            String message = reasonTextField.getText();

            if (requestSelected instanceof VeterinaryWorkRequest) {

                VeterinaryWorkRequest request = (VeterinaryWorkRequest) requestSelected;
                vetRequestNew = new VeterinaryWorkRequest();
                vet = (VeterinaryDoctor) request.getVetDoc();
                vetRequestNew.setNextAppointmentDateVeterinary(dateAppointment);
                vetRequestNew.setDayOfAppointment(day);
                vetRequestNew.setVeterinaryName(vet.getVetName());
                vetRequestNew.setVetDoc(vet);
                Pet pet = (Pet) request.getPet();
                vetRequestNew.setSender(userAccount);
                vetRequestNew.setPet(pet);
                vetRequestNew.setFutureStatus("Pending");
                vetRequestNew.setFutureMessage(message);
                vetRequestNew.setPetOwner(request.getPetOwner());

                Organization org = null;
                Enterprise ent = null;
                UserAccount user1 = null;
                for (Network network : business.getNetworkList()) {
                    for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                        ent = enterprise;
                        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                            if (organization instanceof VeterinaryDoctorOrganization) {
                                org = organization;
                                user1 = request.getSender();
                                break;

                            }
                        }
                    }
                }

                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(vetRequestNew);
                    user1.getWorkQueue().getWorkRequestList().add(vetRequestNew);
                }

            } else if (requestSelected instanceof KennelWorkRequest) {
                KennelWorkRequest request = (KennelWorkRequest) requestSelected;
                kenRequestNew = new KennelWorkRequest();
                kenRequestNew.setKennelName(request.getKennelName());
                kenRequestNew.setAppointmentDay(day);
                kenRequestNew.setAppointmentDate(dateAppointment);
                Pet pet = (Pet) request.getPet();
                kenRequestNew.setSender(userAccount);
                kenRequestNew.setPet(pet);
                kenRequestNew.setFutureMessage(message);
                kenRequestNew.setFutureStatus("Pending");
                kenRequestNew.setVetPerson(userAccount.getPerson());

                Organization org = null;
                Enterprise ent = null;
                UserAccount user1 = null;
                for (Network network : business.getNetworkList()) {
                    for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                        ent = enterprise;
                        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                            if (organization instanceof VeterinaryDoctorOrganization) {
                                org = organization;
                                break;

                            }
                        }
                    }
                }

                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(kenRequestNew);
                    userAccount.getWorkQueue().getWorkRequestList().add(kenRequestNew);
                }

            }
        }

        if (vetRequestNew instanceof VeterinaryWorkRequest) {

            VeterinaryAppointmentScheduleJPanel appoint = new VeterinaryAppointmentScheduleJPanel(userProcessContainer, userAccount, organization, enterprise, vetRequestNew, kenRequestNew, this);
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            userProcessContainer.add("Future Dates", appoint);
            layout.next(userProcessContainer);
        } else if (kenRequestNew instanceof KennelWorkRequest) {
            VeterinaryAppointmentScheduleJPanel appoint = new VeterinaryAppointmentScheduleJPanel(userProcessContainer, userAccount, organization, enterprise, vetRequestNew, kenRequestNew, this);
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            userProcessContainer.add("Future Dates", appoint);
            layout.next(userProcessContainer);

        }

    }//GEN-LAST:event_bookFutureButtonActionPerformed

    private void viewFeedbackButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewFeedbackButtonActionPerformed
        FeedbackJPanel feedback = new FeedbackJPanel(userProcessContainer, userAccount, organization, enterprise, request);
        userProcessContainer.add("Feedback", feedback);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_viewFeedbackButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed

    //Validates and deletes a request based on the status of the request
    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        int selectedRow = workRequestJTable.getSelectedRow();
        VeterinaryWorkRequest requestFound = null;
        KennelWorkRequest requestFound1 = null;
        PetOwnerOrganization petOrg = null;
        KennelOrganization kenOrg = null;
        UserAccount user = null;
        int count = 0;
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row to delete the request");
            return;
        } else {
            String person = (String) workRequestJTable.getValueAt(selectedRow, 1);

            for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
                if (request instanceof VeterinaryWorkRequest) {
                    VeterinaryWorkRequest req = (VeterinaryWorkRequest) request;
                    if (req.getSender().getPerson().getName().equals(person)) {
                        requestFound = req;
                        if (requestFound.getStatus().equals("Request deleted")) {
                            JOptionPane.showMessageDialog(null, "You cannot delete a request which is already deleted");
                            return;
                        }
                        if (requestFound.getStatus().equals("Completed")) {
                            JOptionPane.showMessageDialog(null, "You cannot delete a request which is completed");
                            return;
                        } else {
                            requestFound.setStatus("Request deleted");
                            populateTable();
                            return;
                        }

                    }
                } else if (request instanceof KennelWorkRequest) {
                    KennelWorkRequest req = (KennelWorkRequest) request;
                    if (req.getSender().getPerson().getName().equals(person)) {
                        requestFound1 = req;
                        if (requestFound1.getStatus().equals("Request deleted")) {
                            JOptionPane.showMessageDialog(null, "You cannot delete a request which is already deleted");
                            return;
                        }
                        if (requestFound1.getStatus().equals("Completed")) {
                            JOptionPane.showMessageDialog(null, "You cannot delete a request which is completed");
                            return;
                        } else {
                            requestFound1.setStatus("Request deleted");
                            populateTable();
                            return;
                        }

                    }

                }
            }
        }
    }//GEN-LAST:event_deleteButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignJButton;
    private javax.swing.JButton backButton;
    private javax.swing.JButton bookFutureButton;
    private javax.swing.JButton deleteButton;
    private com.toedter.calendar.JDateChooser jDateChooserAppointment;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton processJButton;
    private javax.swing.JTextField reasonTextField;
    private javax.swing.JButton refreshJButton;
    private javax.swing.JButton viewFeedbackButton;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables

    //Performs field level validations
    public int validateFields() {
        Date vetDate = jDateChooserAppointment.getDate();
        String vetDateSubstring = null;
        Date date = new Date();
        Validation validation = new Validation();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd");

        if (vetDate != null) {
            vetDateSubstring = String.valueOf(vetDate).substring(0, 10);
        }

        String dateSubstring = String.valueOf(date).substring(0, 10);

        if (vetDate == null) {
            JOptionPane.showMessageDialog(null, "Please select a date of appointment");
            return 1;
        }

        if (String.valueOf(vetDate).length() != 28) {
            JOptionPane.showMessageDialog(null, "The date format enetered is incorrect.");
            return 1;
        }
        if (vetDateSubstring != null && vetDateSubstring.equals(dateSubstring)) {
            return 0;
        } else if (vetDate.before(date)) {
            JOptionPane.showMessageDialog(null, "Please select today's date or any date after today's date");
            return 1;
        } else if (reasonTextField.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter a reason");
            return 1;
        } else if (validation.validateStreet(reasonTextField.getText()) == 1) {
            JOptionPane.showMessageDialog(null, "Please do not make use of special characters.Alphabets and numbers are allowed.");
            return 1;
        }

        return 0;
    }

}
