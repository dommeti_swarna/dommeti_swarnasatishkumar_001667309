/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Pet;

import static Business.Role.Role.RoleType.Sensor;
import Business.Sensor.Sensor;
import java.util.Date;

/**
 *
 * @author Swarna 
 * This class initializes the pet's location tracking private
 * variables using getters and setters.
 */
public class Tracker {

    private Date timestamp;
    private Pet pet;
    private String trackingsStatus;
    private float trackingValue;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String getTrackingsStatus() {
        return trackingsStatus;
    }

    public void setTrackingsStatus(String trackingsStatus) {
        this.trackingsStatus = trackingsStatus;
    }

    public float getTrackingValue() {
        return trackingValue;
    }

    public void setTrackingValue(float trackingValue) {
        this.trackingValue = trackingValue;
    }
}
