/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Pet;

import static Business.Role.Role.RoleType.Sensor;
import Business.Sensor.Sensor;
import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class adds the vital sign details of a pet to the
 * ArrayList of VitalSign.
 */
public class VitalSignDirectory {

    private ArrayList<VitalSign> vitalSignList;

    public VitalSignDirectory() {
        vitalSignList = new ArrayList<>();
    }

    public ArrayList<VitalSign> getVitalSignList() {
        return vitalSignList;
    }

    public void setVitalSignList(ArrayList<VitalSign> vitalSignList) {
        this.vitalSignList = vitalSignList;
    }

    public VitalSign addVitalSigns(VitalSign vitalSign) {
        vitalSignList.add(vitalSign);
        return vitalSign;
    }

    public VitalSign addVital() {
        VitalSign vs = new VitalSign();
        vitalSignList.add(vs);
        return vs;
    }
}
