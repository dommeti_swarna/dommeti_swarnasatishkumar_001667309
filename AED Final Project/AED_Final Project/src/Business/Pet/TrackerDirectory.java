/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Pet;

import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class adds the tracking details of a pet based on its
 * location to the ArrayList of Tracker.
 */
public class TrackerDirectory {

    private ArrayList<Tracker> trackerList;

    public TrackerDirectory() {
        trackerList = new ArrayList<>();
    }

    public ArrayList<Tracker> getTrackerList() {
        return trackerList;
    }

    public void setTrackerList(ArrayList<Tracker> trackerList) {
        this.trackerList = trackerList;
    }

    public Tracker addTrackingDetails(Tracker tracker) {
        trackerList.add(tracker);
        return tracker;
    }
}
