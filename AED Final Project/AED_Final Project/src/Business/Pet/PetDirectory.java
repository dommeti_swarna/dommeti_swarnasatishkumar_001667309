/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Pet;

import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class adds a pet to the ArrayList of Pets.
 */
public class PetDirectory {

    private ArrayList<Pet> petList;

    public PetDirectory() {
        petList = new ArrayList<>();
    }

    public ArrayList<Pet> getPetList() {
        return petList;
    }

    public void setPetList(ArrayList<Pet> petList) {
        this.petList = petList;
    }

    public Pet addPet(Pet pet) {
        petList.add(pet);
        return pet;
    }
}
