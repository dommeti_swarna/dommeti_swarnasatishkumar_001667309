/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Pet;

import Business.PetOwner.PetOwner;
import static Business.Role.Role.RoleType.PetOwner;
import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class is used to initialize private variables of Pet
 * using getters and setters.
 */
public class Pet {

    private String petName;
    private int petTagID;
    private String petType;
    private String petSize;
    private int petAge;
    private String petStatus;
    private VitalSignDirectory vitalSignList;
    private TrackerDirectory trackerDirectory;

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public int getPetTagID() {
        return petTagID;
    }

    public void setPetTagID(int petTagID) {
        this.petTagID = petTagID;
    }

    public String getPetType() {
        return petType;
    }

    public void setPetType(String petType) {
        this.petType = petType;
    }

    public String getPetSize() {
        return petSize;
    }

    public void setPetSize(String petSize) {
        this.petSize = petSize;
    }

    public int getPetAge() {
        return petAge;
    }

    public void setPetAge(int petAge) {
        this.petAge = petAge;
    }

    public String getPetStatus() {
        return petStatus;
    }

    public void setPetStatus(String petStatus) {
        this.petStatus = petStatus;
    }

    public VitalSignDirectory getVitalSignList() {
        return this.vitalSignList;
    }

    public void setVitalSignList(VitalSignDirectory vitalSignList) {
        this.vitalSignList = vitalSignList;
    }

    public TrackerDirectory getTrackerDirectory() {
        return trackerDirectory;
    }

    public void setTrackerDirectory(TrackerDirectory trackerDirectory) {
        this.trackerDirectory = trackerDirectory;
    }

    @Override
    public String toString() {
        return petName;
    }
}
