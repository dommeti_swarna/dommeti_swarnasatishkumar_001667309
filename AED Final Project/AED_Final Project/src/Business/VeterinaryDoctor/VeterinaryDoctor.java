/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.VeterinaryDoctor;

import Business.Person.Person;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author Swarna 
 * This class initializes all the private variables of Veterinary
 * Doctor using getters and setters. It performs comparison between Veterinary
 * Doctors based on ratings and sorts in descending order using compareTo
 * function. It also performs comparison between the size of feedback given to
 * the Veterinary Doctor and sorts in descending order using compare function
 */
public class VeterinaryDoctor extends Person implements Comparable<VeterinaryDoctor>, Comparator<VeterinaryDoctor> {

    private String vetName;
    private String vetStreet;
    private int vetID;
    private int vetNo;
    private int counter = 0;
    private int rating;
    private String vetCity;
    private int vetPhoneNumber;
    private int extension;
    private String vetDegree;
    private String mondayTime;
    private String tuesdayTime;
    private String wednesdayTime;
    private String thursdayTime;
    private String fridayTime;
    private String saturdayTime;
    private String sundayTime;
    private ArrayList<String> feedback;

    public VeterinaryDoctor() {
        counter++;
        vetID = counter;
    }

    public String getVetName() {
        return vetName;
    }

    public void setVetName(String vetName) {
        this.vetName = vetName;
    }

    public String getVetStreet() {
        return vetStreet;
    }

    public void setVetStreet(String vetStreet) {
        this.vetStreet = vetStreet;
    }

    public int getVetID() {
        return vetID;
    }

    public void setVetID(int vetID) {
        this.vetID = vetID;
    }

    public int getVetNo() {
        return vetNo;
    }

    public void setVetNo(int vetNo) {
        this.vetNo = vetNo;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getVetCity() {
        return vetCity;
    }

    public void setVetCity(String vetCity) {
        this.vetCity = vetCity;
    }

    public int getVetPhoneNumber() {
        return vetPhoneNumber;
    }

    public void setVetPhoneNumber(int vetPhoneNumber) {
        this.vetPhoneNumber = vetPhoneNumber;
    }

    public String getVetDegree() {
        return vetDegree;
    }

    public void setVetDegree(String vetDegree) {
        this.vetDegree = vetDegree;
    }

    public String getMondayTime() {
        return mondayTime;
    }

    public void setMondayTime(String mondayTime) {
        this.mondayTime = mondayTime;
    }

    public String getTuesdayTime() {
        return tuesdayTime;
    }

    public void setTuesdayTime(String tuesdayTime) {
        this.tuesdayTime = tuesdayTime;
    }

    public String getWednesdayTime() {
        return wednesdayTime;
    }

    public void setWednesdayTime(String wednesdayTime) {
        this.wednesdayTime = wednesdayTime;
    }

    public String getThursdayTime() {
        return thursdayTime;
    }

    public void setThursdayTime(String thursdayTime) {
        this.thursdayTime = thursdayTime;
    }

    public String getSaturdayTime() {
        return saturdayTime;
    }

    public void setSaturdayTime(String saturdayTime) {
        this.saturdayTime = saturdayTime;
    }

    public String getSundayTime() {
        return sundayTime;
    }

    public void setSundayTime(String sundayTime) {
        this.sundayTime = sundayTime;
    }

    public String getFridayTime() {
        return fridayTime;
    }

    public void setFridayTime(String fridayTime) {
        this.fridayTime = fridayTime;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public ArrayList<String> getFeedback() {
        return feedback;
    }

    public void setFeedback(ArrayList<String> feedback) {
        this.feedback = feedback;
    }

    public int getExtension() {
        return extension;
    }

    public void setExtension(int extension) {
        this.extension = extension;
    }

    @Override
    public int compareTo(VeterinaryDoctor o) {

        if (this.getRating() > o.getRating()) {
            return -1;
        } else if (this.getRating() < o.getRating()) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int compare(VeterinaryDoctor o1, VeterinaryDoctor o2) {

        if ((o1.getFeedback().size()) > (o2.getFeedback().size())) {
            return -1;
        } else if ((o1.getFeedback().size()) < (o2.getFeedback().size())) {
            return 1;
        } else {
            return 0;
        }
    }

}
