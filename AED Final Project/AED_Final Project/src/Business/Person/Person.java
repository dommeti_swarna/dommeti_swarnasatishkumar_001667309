/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Person;

/**
 *
 * @author Swarna 
 * This class is used to initialize all the private variables of
 * Person using getters and setters.
 */
public class Person {

    private String name;
    private int personID;
    private static int counter = 0;

    public Person() {
        counter++;
        personID = counter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPersonID() {
        return personID;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }

    public String toString() {
        return name;
    }
}
