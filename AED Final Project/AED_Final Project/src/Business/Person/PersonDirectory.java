/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Person;

import Business.Enterprise.Enterprise;
import Business.Enterprise.KennelEnterprise;
import Business.Enterprise.PetCareEnterprise;
import Business.Enterprise.VeterinaryCentersEnterprise;
import Business.Kennel.Kennel;
import Business.Organization.KennelOrganization;
import Business.Organization.Organization;
import Business.Organization.PetOwnerOrganization;
import Business.Organization.VeterinaryDoctorOrganization;
import Business.PetOwner.PetOwner;
import Business.VeterinaryDoctor.VeterinaryDoctor;
import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class creates Person based on the name of the
 * Organization and Enterprise passed as parameter and adds to the ArrayList of
 * Person.
 */
public class PersonDirectory {

    private ArrayList<Person> personList;

    public PersonDirectory() {
        personList = new ArrayList<>();
    }

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }

    public Person createPerson(String name, Organization organization, Enterprise enterprise) {
        if (enterprise instanceof PetCareEnterprise) {
            if (organization instanceof PetOwnerOrganization) {
                Person p = new PetOwner();
                p.setName(name);
                personList.add(p);
                return p;
            } else {
                Person p = new PetOwner();
                p.setName(name);
                personList.add(p);
                return p;
            }
        } else if (enterprise instanceof VeterinaryCentersEnterprise) {
            if (organization instanceof VeterinaryDoctorOrganization) {
                Person p = new VeterinaryDoctor();
                p.setName(name);
                personList.add(p);
                return p;

            } else {
                Person p = new PetOwner();
                p.setName(name);
                personList.add(p);
                return p;
            }
        } else if (enterprise instanceof KennelEnterprise) {
            if (organization instanceof KennelOrganization) {
                Person p = new Kennel();
                p.setName(name);
                personList.add(p);
                return p;
            } else {
                Person p = new PetOwner();
                p.setName(name);
                personList.add(p);
                return p;
            }
        }
        return null;
    }
}
