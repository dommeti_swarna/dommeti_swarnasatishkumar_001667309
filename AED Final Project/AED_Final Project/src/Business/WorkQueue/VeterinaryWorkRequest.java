/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Person.Person;
import Business.Pet.Pet;

/**
 *
 * @author Swarna 
 * This class initializes all the private variables of Veterinary
 * Work Request using getters and setters.
 */
public class VeterinaryWorkRequest extends WorkRequest {

    private String testResults;
    private Person petOwner;
    private Pet pet;
    private Person vetDoc;
    private String dateOfAppointment;
    private String dayOfAppointment;
    private String nextAppointmentDatePetOwner;
    private String nextAppointmentDateVeterinary;
    private String petOwnerName;
    private String veterinaryName;
    private String futureStatus;
    private String futureMessage;

    public String getTestResults() {
        return testResults;
    }

    public void setTestResults(String testResults) {
        this.testResults = testResults;
    }

    public Person getPetOwner() {
        return petOwner;
    }

    public void setPetOwner(Person petOwner) {
        this.petOwner = petOwner;
    }

    public String getDateOfAppointment() {
        return dateOfAppointment;
    }

    public void setDateOfAppointment(String dateOfAppointment) {
        this.dateOfAppointment = dateOfAppointment;
    }

    public String getDayOfAppointment() {
        return dayOfAppointment;
    }

    public void setDayOfAppointment(String dayOfAppointment) {
        this.dayOfAppointment = dayOfAppointment;
    }

    public String getPetOwnerName() {
        return petOwnerName;
    }

    public void setPetOwnerName(String petOwnerName) {
        this.petOwnerName = petOwnerName;
    }

    public String getNextAppointmentDatePetOwner() {
        return nextAppointmentDatePetOwner;
    }

    public void setNextAppointmentDatePetOwner(String nextAppointmentDatePetOwner) {
        this.nextAppointmentDatePetOwner = nextAppointmentDatePetOwner;
    }

    public String getNextAppointmentDateVeterinary() {
        return nextAppointmentDateVeterinary;
    }

    public void setNextAppointmentDateVeterinary(String nextAppointmentDateVeterinary) {
        this.nextAppointmentDateVeterinary = nextAppointmentDateVeterinary;
    }

    public String getVeterinaryName() {
        return veterinaryName;
    }

    public void setVeterinaryName(String veterinaryName) {
        this.veterinaryName = veterinaryName;
    }

    public Person getVetDoc() {
        return vetDoc;
    }

    public void setVetDoc(Person vetDoc) {
        this.vetDoc = vetDoc;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String getFutureStatus() {
        return futureStatus;
    }

    public void setFutureStatus(String futureStatus) {
        this.futureStatus = futureStatus;
    }

    public String getFutureMessage() {
        return futureMessage;
    }

    public void setFutureMessage(String futureMessage) {
        this.futureMessage = futureMessage;
    }

    @Override
    public String toString() {
        return futureMessage;
    }

}
