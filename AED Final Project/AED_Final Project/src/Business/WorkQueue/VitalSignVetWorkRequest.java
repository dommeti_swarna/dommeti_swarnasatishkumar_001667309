/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Pet.Pet;
import Business.UserAccount.UserAccount;
import Business.VeterinaryDoctor.VeterinaryDoctor;

/**
 *
 * @author Swarna 
 * This class initializes all the private variables of Vital Sign
 * Vet Work Request using getters and setters. It compares the integer value of
 * the request entered and sorts it in descending order using compareTo function
 */
public class VitalSignVetWorkRequest extends WorkRequest implements Comparable<VitalSignVetWorkRequest> {

    private Pet pet;
    private int respiratoryRate;
    private int heartRate;
    private int temperature;
    private UserAccount petOwner;
    private String sensorStatus;
    private int requestEntered;
    private String date;
    private VeterinaryDoctor veterinaryDoctor;

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public int getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(int respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public UserAccount getPetOwner() {
        return petOwner;
    }

    public void setPetOwner(UserAccount petOwner) {
        this.petOwner = petOwner;
    }

    public String getSensorStatus() {
        return sensorStatus;
    }

    public void setSensorStatus(String sensorStatus) {
        this.sensorStatus = sensorStatus;
    }

    public int getRequestEntered() {
        return requestEntered;
    }

    public void setRequestEntered(int requestEntered) {
        this.requestEntered = requestEntered;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public VeterinaryDoctor getVeterinaryDoctor() {
        return veterinaryDoctor;
    }

    public void setVeterinaryDoctor(VeterinaryDoctor veterinaryDoctor) {
        this.veterinaryDoctor = veterinaryDoctor;
    }

    @Override
    public int compareTo(VitalSignVetWorkRequest o) {

        if (this.getRequestEntered() > o.getRequestEntered()) {
            return -1;
        } else if (this.getRequestEntered() < o.getRequestEntered()) {
            return 1;
        } else {
            return 0;
        }
    }
}
