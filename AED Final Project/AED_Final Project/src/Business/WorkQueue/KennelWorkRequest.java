/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Person.Person;
import Business.Pet.Pet;
import java.util.Date;

/**
 *
 * @author Swarna 
 * This class initializes all the private variables of Kennel
 * Work Request using getters and setters.
 */
public class KennelWorkRequest extends WorkRequest {

    private Pet pet;
    private String appointmentDate;
    private Person vetPerson;
    private String testResults;
    private String appointmentDay;
    private String kennelName;
    private String futureMessage;
    private String futureStatus;
    private String ownerMessage;
    private Date nextDate;

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public Person getVetPerson() {
        return vetPerson;
    }

    public void setVetPerson(Person vetPerson) {
        this.vetPerson = vetPerson;
    }

    public String getTestResults() {
        return testResults;
    }

    public void setTestResults(String testResults) {
        this.testResults = testResults;
    }

    public String getAppointmentDay() {
        return appointmentDay;
    }

    public void setAppointmentDay(String appointmentDay) {
        this.appointmentDay = appointmentDay;
    }

    public String getKennelName() {
        return kennelName;
    }

    public void setKennelName(String kennelName) {
        this.kennelName = kennelName;
    }

    public String getFutureMessage() {
        return futureMessage;
    }

    public void setFutureMessage(String futureMessage) {
        this.futureMessage = futureMessage;
    }

    public String getFutureStatus() {
        return futureStatus;
    }

    public void setFutureStatus(String futureStatus) {
        this.futureStatus = futureStatus;
    }

    public String getOwnerMessage() {
        return ownerMessage;
    }

    public void setOwnerMessage(String ownerMessage) {
        this.ownerMessage = ownerMessage;
    }

    public Date getNextDate() {
        return nextDate;
    }

    public void setNextDate(Date nextDate) {
        this.nextDate = nextDate;
    }

    @Override
    public String toString() {
        return futureMessage;
    }

}
