/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Kennel.Kennel;
import Business.Person.Person;
import Business.Pet.Pet;

/**
 *
 * @author Swarna 
 * This class initializes all the private variables of Pet Owner
 * Kennel Work Request using getters and setters.
 */
public class PetOwnerKennelWorkRequest extends WorkRequest {

    private String availabilityToDate;
    private String availabilityFromDate;
    private String costPerDay;
    private String petPerDay;
    private String petFromDate;
    private String petToDate;
    private Pet pet;
    private Kennel kennel;
    private String petOwner;

    public String getAvailabilityToDate() {
        return availabilityToDate;
    }

    public void setAvailabilityToDate(String availabilityToDate) {
        this.availabilityToDate = availabilityToDate;
    }

    public String getAvailabilityFromDate() {
        return availabilityFromDate;
    }

    public void setAvailabilityFromDate(String availabilityFromDate) {
        this.availabilityFromDate = availabilityFromDate;
    }

    public String getCostPerDay() {
        return costPerDay;
    }

    public void setCostPerDay(String costPerDay) {
        this.costPerDay = costPerDay;
    }

    public String getPetPerDay() {
        return petPerDay;
    }

    public void setPetPerDay(String petPerDay) {
        this.petPerDay = petPerDay;
    }

    public String getPetFromDate() {
        return petFromDate;
    }

    public void setPetFromDate(String petFromDate) {
        this.petFromDate = petFromDate;
    }

    public String getPetToDate() {
        return petToDate;
    }

    public void setPetToDate(String petToDate) {
        this.petToDate = petToDate;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Kennel getKennel() {
        return kennel;
    }

    public void setKennel(Kennel kennel) {
        this.kennel = kennel;
    }

    public String getPetOwner() {
        return petOwner;
    }

    public void setPetOwner(String petOwner) {
        this.petOwner = petOwner;
    }

}
