/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Pet.Pet;

/**
 *
 * @author Swarna 
 * This class initializes all the private variables of Pet Owner
 * Work Request using getters and setters.
 */
public class PetOwnerWorkRequest {

    private String dateOfDrop;
    private String dateOfPickUp;
    private Pet pet;
    private String petOwnerName;

    public String getDateOfDrop() {
        return dateOfDrop;
    }

    public void setDateOfDrop(String dateOfDrop) {
        this.dateOfDrop = dateOfDrop;
    }

    public String getDateOfPickUp() {
        return dateOfPickUp;
    }

    public void setDateOfPickUp(String dateOfPickUp) {
        this.dateOfPickUp = dateOfPickUp;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String getPetOwnerName() {
        return petOwnerName;
    }

    public void setPetOwnerName(String petOwnerName) {
        this.petOwnerName = petOwnerName;
    }

}
