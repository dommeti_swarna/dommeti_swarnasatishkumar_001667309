/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.PetHospital;

/**
 *
 * @author Swarna 
 * This class initializes the private variables of Pet Hospital
 * using getters and setters.
 */
public class PetHospital {

    private String hospitalName;
    private String hospitalStreet;
    private int numberOfAmbulance;
    private int hospitalRooms;
    private int availability;
    private String hospitalDoctors;
    private static int counter = 0;
    private int hospitalID;

    public PetHospital() {
        counter++;
        hospitalID = counter;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getHospitalStreet() {
        return hospitalStreet;
    }

    public void setHospitalStreet(String hospitalStreet) {
        this.hospitalStreet = hospitalStreet;
    }

    public int getNumberOfAmbulance() {
        return numberOfAmbulance;
    }

    public void setNumberOfAmbulance(int numberOfAmbulance) {
        this.numberOfAmbulance = numberOfAmbulance;
    }

    public int getHospitalRooms() {
        return hospitalRooms;
    }

    public void setHospitalRooms(int hospitalRooms) {
        this.hospitalRooms = hospitalRooms;
    }

    public int getAvailability() {
        return availability;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }

    public String getHospitalDoctors() {
        return hospitalDoctors;
    }

    public void setHospitalDoctors(String hospitalDoctors) {
        this.hospitalDoctors = hospitalDoctors;
    }

}
