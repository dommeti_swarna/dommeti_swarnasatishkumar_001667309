 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Enterprise.Enterprise;
import Business.Person.Person;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class creates user accounts as well as validates user
 * name and password to check if there is any account having the same name and
 * password existing. After validation it adds the user account to the ArrayList
 * of UserAccount.
 */
public class UserAccountDirectory {

    private ArrayList<UserAccount> userAccountList;

    public UserAccountDirectory() {
        userAccountList = new ArrayList<>();
    }

    public ArrayList<UserAccount> getUserAccountList() {
        return userAccountList;
    }

    public void setUserAccountList(ArrayList<UserAccount> userAccountList) {
        this.userAccountList = userAccountList;
    }

    public UserAccount createUserAccount(String userName, String password, Person person, Role role, Enterprise enterprise) {
        UserAccount userAccount = new UserAccount();
        userAccount.setPassword(password);
        userAccount.setUserName(userName);
        userAccount.setPerson(person);
        userAccount.setRole(role);
        userAccount.setEnterprise(enterprise);
        userAccountList.add(userAccount);
        return userAccount;
    }

    public UserAccount authenticateUser(String userName, String password) {
        for (UserAccount userAccount : userAccountList) {
            if ((userAccount.getUserName().equals(userName)) && (userAccount.getPassword().equals(password))) {
                return userAccount;
            }
        }
        return null;
    }

}
