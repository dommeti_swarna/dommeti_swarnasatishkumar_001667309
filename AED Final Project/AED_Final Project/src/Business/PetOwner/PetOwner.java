/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.PetOwner;

import Business.Person.Person;
import Business.Pet.Pet;
import Business.Pet.PetDirectory;
import static Business.Role.Role.RoleType.VeterinaryDoctor;
import Business.VeterinaryDoctor.VeterinaryDoctor;

/**
 *
 * @author Swarna 
 * This class initializes the private variables of Pet Owner
 * using getters and setters.
 */
public class PetOwner extends Person {

    private String ownerName;
    private String ownerStreetName;
    private int ownerID;
    private String ownerCity;
    private int phoneNumber;
    private int counter = 0;
    private PetDirectory petDirectory;
    private static String URL;
    private int xCoordinate;
    private int yCoordinate;
    private int radius;
    private String mailID;
    private int extention;

    public PetOwner() {
        counter++;
        ownerID = counter;
        petDirectory = new PetDirectory();
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public String getOwnerStreetName() {
        return ownerStreetName;
    }

    public void setOwnerStreetName(String ownerStreetName) {
        this.ownerStreetName = ownerStreetName;
    }

    public String getOwnerCity() {
        return ownerCity;
    }

    public void setOwnerCity(String ownerCity) {
        this.ownerCity = ownerCity;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public PetDirectory getPetDirectory() {
        return petDirectory;
    }

    public void setPetDirectory(PetDirectory petDirectory) {
        this.petDirectory = petDirectory;
    }

    public static String getURL() {
        return URL;
    }

    public static void setURL(String URL) {
        PetOwner.URL = URL;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(int xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(int yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getMailID() {
        return mailID;
    }

    public void setMailID(String mailID) {
        this.mailID = mailID;
    }

    public int getExtention() {
        return extention;
    }

    public void setExtention(int extention) {
        this.extention = extention;
    }

}
