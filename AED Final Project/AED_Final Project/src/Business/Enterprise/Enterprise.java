/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.Role.AdminRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 * @author Swarna 
 * This class is used to create Enterprises using enum. The class
 * is an abstract class and its definition is implemented in the individual
 * enterprise classes.
 *
 */
public abstract class Enterprise extends Organization {

    private EnterpriseType type;
    private OrganizationDirectory organizationDirectory;

    public Enterprise(String name, EnterpriseType type) {
        super(name);
        this.type = type;
        organizationDirectory = new OrganizationDirectory();
    }

    public enum EnterpriseType {

        PetCare("Pet Care System"),
        VeterinaryCenters("Veterinary Centers"),
        Kennel("Kennel Centers");

        private String value;

        public String getValue() {
            return value;
        }

        private EnterpriseType(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    public void setOrganizationDirectory(OrganizationDirectory organizationDirectory) {
        this.organizationDirectory = organizationDirectory;
    }

    public EnterpriseType getType() {
        return type;
    }

    public void setType(EnterpriseType type) {
        this.type = type;
    }

}
