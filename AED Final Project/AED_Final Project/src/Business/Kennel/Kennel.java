/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Kennel;

import Business.Person.Person;
import Business.VeterinaryDoctor.VeterinaryDoctor;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Swarna 
 * This class is used to initialize various private variables of
 * Kennel using getter and setters.
 */
public class Kennel extends Person implements Comparable<Kennel>{

    private String kennelName;
    private String kennelCity;
    private String kennelStreet;
    private Date fromDate;
    private Date toDate;
    private int petsPerDay;
    private int phoneNumber;
    private int costPerDay;
    private int extension;
    private ArrayList<String> imageList;

    public Kennel() {
        imageList = new ArrayList<>();
    }

    public String getKennelName() {
        return kennelName;
    }

    public void setKennelName(String kennelName) {
        this.kennelName = kennelName;
    }

    public String getKennelCity() {
        return kennelCity;
    }

    public void setKennelCity(String kennelCity) {
        this.kennelCity = kennelCity;
    }

    public String getKennelStreet() {
        return kennelStreet;
    }

    public void setKennelStreet(String kennelStreet) {
        this.kennelStreet = kennelStreet;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public int getPetsPerDay() {
        return petsPerDay;
    }

    public void setPetsPerDay(int petsPerDay) {
        this.petsPerDay = petsPerDay;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getCostPerDay() {
        return costPerDay;
    }

    public void setCostPerDay(int costPerDay) {
        this.costPerDay = costPerDay;
    }

    public ArrayList<String> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<String> imageList) {
        this.imageList = imageList;
    }

    public int getExtension() {
        return extension;
    }

    public void setExtension(int extension) {
        this.extension = extension;
    }
    
    @Override
    public int compareTo(Kennel k) {

        if (this.getCostPerDay() > k.getCostPerDay()) {
            return -1;
        } else if (this.getCostPerDay()< k.getCostPerDay()) {
            return 1;
        } else {
            return 0;
        }
    }
}
