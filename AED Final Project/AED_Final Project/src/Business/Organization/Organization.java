/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Person.PersonDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class is creates different Organizations using enum. It
 * is an abstract class whose implementation is provided in the individual
 * organization class.
 */
public abstract class Organization {

    private PersonDirectory personDirectory;
    private UserAccountDirectory userAccountDirectory;
    private WorkQueue workQueue;
    private int organizationID;
    private static int counter;
    private String name;

    public enum Type {

        AdminOrganization("Admin Organization"),
        PetOwnerOrganization("Pet Owner Organization");

        private String value;

        public String getValue() {
            return value;
        }

        private Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public enum TypeKennel {

        KennelOrganization("Kennel Organization");

        private String value;

        public String getValue() {
            return value;
        }

        private TypeKennel(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public enum TypeVeterinary {

        VeterinaryDoctorOrganization("Veterinary Doctor Organization");

        private String value;

        public String getValue() {
            return value;
        }

        private TypeVeterinary(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        personDirectory = new PersonDirectory();
        workQueue = new WorkQueue();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        counter++;
    }

    public abstract ArrayList<Role> getSupportedRole();

    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(int organizationID) {
        this.organizationID = organizationID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
