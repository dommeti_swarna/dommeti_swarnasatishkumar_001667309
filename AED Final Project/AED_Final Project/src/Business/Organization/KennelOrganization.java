/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.KennelRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class adds Kennel users to ArrayList of Kennel Role.
 */
public class KennelOrganization extends Organization {

    public KennelOrganization() {
        super(TypeKennel.KennelOrganization.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new KennelRole());
        return roleList;
    }

}
