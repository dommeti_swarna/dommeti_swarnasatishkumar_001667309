/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.VeterinaryDoctorRole;
import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class adds Veterinary Doctor users to ArrayList of
 * Veterinary Doctor Role
 */
public class VeterinaryDoctorOrganization extends Organization {

    public VeterinaryDoctorOrganization() {
        super(TypeVeterinary.VeterinaryDoctorOrganization.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new VeterinaryDoctorRole());
        return roleList;
    }

}
