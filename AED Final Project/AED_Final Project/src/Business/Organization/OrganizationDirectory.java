/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Enterprise.Enterprise;
import Business.Enterprise.Enterprise.EnterpriseType;
import Business.Organization.Organization.Type;
import Business.Organization.Organization.TypeKennel;
import Business.Organization.Organization.TypeVeterinary;
import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class creates Organization based on the type of
 * Enterprise and Organization passed in the parameter and adds it to the
 * ArrayList of Organization.
 */
public class OrganizationDirectory {

    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public void setOrganizationList(ArrayList<Organization> organizationList) {
        this.organizationList = organizationList;
    }

    public Organization createOrganization(Type type, EnterpriseType enterpriseType) {
        Organization organization = null;

        if (enterpriseType.getValue().equals(EnterpriseType.PetCare.getValue())) {
            if (type.getValue().equals(Type.PetOwnerOrganization.getValue())) {
                organization = new PetOwnerOrganization();
                organizationList.add(organization);
            }

        }
        return null;
    }

    public Organization createOrganization(TypeVeterinary type, EnterpriseType enterpriseType) {
        Organization organization = null;
        if (enterpriseType.getValue().equals(EnterpriseType.VeterinaryCenters.getValue())) {
            if (type.getValue().equals(type.VeterinaryDoctorOrganization.getValue())) {
                organization = new VeterinaryDoctorOrganization();
                organizationList.add(organization);
            }
        }
        return null;
    }

    public Organization createOrganization(TypeKennel type, EnterpriseType enterpriseType) {
        Organization organization = null;
        if (enterpriseType.getValue().equals(EnterpriseType.Kennel.getValue())) {
            if (type.getValue().equals(type.KennelOrganization.getValue())) {
                organization = new KennelOrganization();
                organizationList.add(organization);
            }
        }
        return null;
    }
}
