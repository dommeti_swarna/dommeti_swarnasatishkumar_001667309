/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.PetOwnerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Swarna 
 * This class adds Pet Owner users to ArrayList of Pet Owner Role
 */
public class PetOwnerOrganization extends Organization {

    public PetOwnerOrganization() {
        super(Type.PetOwnerOrganization.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new PetOwnerRole());
        return roleList;
    }
}
