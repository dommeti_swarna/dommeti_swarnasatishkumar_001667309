/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Sensor;

import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathConstants;
import org.w3c.dom.Document;

/**
 *
 * @author Swarna 
 * This class fetches the latitude and longitude of a location
 * based on its postcode using Google Maps API
 */
public class FetchingLatLongValues {

    private String postcode;
    private String latitudeLongitude[];

    public FetchingLatLongValues() throws Exception {
        System.setProperty("java.net.useSystemProxies", "true");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            postcode = br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(FetchingLatLongValues.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            latitudeLongitude = getLatitudeLongitudePositions(postcode);
        } catch (Exception ex) {
            Logger.getLogger(FetchingLatLongValues.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Latitude: " + latitudeLongitude[0] + " and Longitude: " + latitudeLongitude[1]);
    }

    public static String[] getLatitudeLongitudePositions(String address) throws Exception {
        int responseCode = 0;
        String api = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + URLEncoder.encode(address, "UTF-8") + "&sensor=true";
        //System.out.println("URL : "+api);
        URL url = new URL(api);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.connect();
        responseCode = httpConnection.getResponseCode();
        if (responseCode == 200) {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();;
            Document document = builder.parse(httpConnection.getInputStream());
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("/GeocodeResponse/status");
            String status = (String) expr.evaluate(document, XPathConstants.STRING);
            if (status.equals("OK")) {
                expr = xpath.compile("//geometry/location/lat");
                String latitude = (String) expr.evaluate(document, XPathConstants.STRING);
                expr = xpath.compile("//geometry/location/lng");
                String longitude = (String) expr.evaluate(document, XPathConstants.STRING);
                return new String[]{latitude, longitude};
            } else {
                throw new Exception("Error from the API - response status: " + status);
            }
        }
        return null;
    }
}
