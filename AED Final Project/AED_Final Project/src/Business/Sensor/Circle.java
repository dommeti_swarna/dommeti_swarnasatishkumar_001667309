/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Sensor;

import Business.PetOwner.PetOwner;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import javax.swing.JComponent;

/**
 *
 * @author Swarna 
 * This class is used to draw a circle around a point to define a
 * secured perimeter for the pet.
 */
public class Circle extends JComponent {

    private int radius;
    private int xCoordinate;
    private int yCoordinate;

    public Circle(PetOwner owner) {
        xCoordinate = owner.getxCoordinate();
        yCoordinate = owner.getyCoordinate();
        radius = owner.getRadius();
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.drawOval(xCoordinate, yCoordinate, radius, radius);
    }
}
