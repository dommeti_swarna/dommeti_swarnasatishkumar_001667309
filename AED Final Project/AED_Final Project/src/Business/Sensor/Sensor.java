/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Sensor;

import java.util.Random;

/**
 *
 * @author Swarna 
 * This class generates random values for different vital signs
 * using Random value generator class.
 */
public class Sensor {

    public int calHeartRate() {
        Random r = new Random();
        int low = 70;
        int high = 160;
        int result = r.nextInt(high - low) + low;
        return result;
    }

    public double calTemeperature() {
        Random r = new Random();
        double low = (float) 100.5;
        double high = (float) 102.5;
        double result = (r.nextDouble() * high) + low;
        return result;
    }

    public int calRespirationRate() {
        Random r = new Random();
        int low = 24;
        int high = 42;
        int result = r.nextInt(high - low) + low;
        return result;
    }
}
