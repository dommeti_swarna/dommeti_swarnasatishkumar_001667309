/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import static Business.Organization.Organization.Type.PetOwnerOrganization;
import Business.Organization.PetOwnerOrganization;
import Business.UserAccount.UserAccount;
import UserInterface.PetOwnerRole.PetOwnerWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author Swarna
 */
public class PetOwnerRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new PetOwnerWorkAreaJPanel(userProcessContainer,userAccount,organization,enterprise,business);
    }
    
}
