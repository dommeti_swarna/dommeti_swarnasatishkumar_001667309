/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Network.Network;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import java.util.ArrayList;

/**
 * @author Swarna 
 * This class is used to implement the Singleton design pattern
 */
public class EcoSystem extends Organization {

    private ArrayList<Network> networkList;
    private static EcoSystem business;

    private EcoSystem() // to invoke the constructor within the class and not outside the class
    {
        super(null);
        networkList = new ArrayList<>();
    }

    public static EcoSystem getInstance() //Singleton design pattern
    {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(ArrayList<Network> networkList) {
        this.networkList = networkList;
    }

    public Network createAndAddNetwork() {
        Network network = new Network();
        networkList.add(network);
        return network;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }

}
