/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Validations;

import javax.swing.JOptionPane;

/**
 *
 * @author Swarna 
 * This class performs all the field level validation of
 * different elements used in the project. It is generic class which is called
 * when validation have to be performed.
 */
public class Validation {

    public int validateString(String s) {
        int counter_check = 0;
        if (s.length() == 0) {
            counter_check = 1;
            return 1;
        } else {
            int length = s.trim().length();
            int counter = 0;
            for (int i = 0; i < length; i++) {
                char character = s.charAt(i);
                int ascii = (int) character;

                if (Character.isDigit(character) || (character == '.') || (ascii >= 33 && ascii <= 47) || (ascii >= 58 && ascii <= 64) || (ascii >= 91 && ascii <= 96) || (ascii >= 123 && ascii <= 126)) {
                    counter++;
                    break;
                } else {
                    counter = 0;
                }
            }
            if (counter > 0) {
                counter_check = 1;
                return 1;
            }
        }
        return 0;
    }

    public int validatePhoneNumber(String a) {
        int counter_check = 0;
        if (a.trim().length() == 0) {
            counter_check = 1;
            return 1;
        } else if (a.length() > 7 || a.length() < 7) {
            counter_check = 1;
            return 1;
        } else {
            int length = a.trim().length();
            int counter = 0;
            for (int i = 0; i < length; i++) {
                char character = a.charAt(i);
                int ascii = (int) character;

                if (Character.isAlphabetic(character) || (character == '.') || (ascii >= 33 && ascii <= 47) || (ascii >= 58 && ascii <= 64) || (ascii >= 91 && ascii <= 96) || (ascii >= 123 && ascii <= 126)) {
                    counter++;
                    break;
                }
                if (Character.isDigit(character)) {
                    int a1 = (int) character;
                    int b = a.indexOf(0);
                    if (a1 == 48 && b == 0) {
                        counter++;
                        break;
                    }
                } else {
                    counter = 0;
                }
            }

            if (counter > 0) {
                counter_check = 1;
                return 1;
            }
        }
        return 0;
    }

    public int validateExtentionCode(String a) {
        int counter_check = 0;
        if (a.trim().length() == 0) {
            counter_check = 1;
            return 1;
        } else if (a.length() > 3 || a.length() < 3) {
            counter_check = 1;
            return 1;
        } else {
            int length = a.trim().length();
            int counter = 0;
            for (int i = 0; i < length; i++) {
                char character = a.charAt(i);
                int ascii = (int) character;

                if (Character.isAlphabetic(character) || (character == '.') || (ascii >= 33 && ascii <= 47) || (ascii >= 58 && ascii <= 64) || (ascii >= 91 && ascii <= 96) || (ascii >= 123 && ascii <= 126)) {
                    counter++;
                    break;
                }
                if (Character.isDigit(character)) {
                    int a1 = (int) character;
                    int b = a.indexOf(0);
                    if (a1 == 48 && b == 0) {
                        counter++;
                        break;
                    }
                } else {
                    counter = 0;
                }
            }

            if (counter > 0) {
                counter_check = 1;
                return 1;
            }
        }
        return 0;
    }

    public int validateMailID(String mail) {
        int counter_check = 0;
        if (mail.trim().length() == 0) {
            counter_check = 1;
            return 1;
        } else {
            boolean findAt = mail.contains("@");
            boolean findDot = mail.contains(".");
            if (findAt == true && findDot == true) {
                int at = mail.indexOf('@');
                int dot = mail.indexOf('.');
                int length = mail.trim().length();
                if (at == 0 || dot == 0) {
                    counter_check = 1;
                    return 1;
                } else if (at <= 0 || dot <= 0) {
                    counter_check = 1;
                    return 1;
                } else if (at >= length || dot >= length) {
                    counter_check = 1;
                    return 1;
                }
            } else {
                counter_check = 1;
                return 1;
            }
        }
        return 0;
    }

    public int validateTagID(String tag) {
        int counter_check = 0;
        if (tag.trim().length() == 0) {
            counter_check = 1;
            return 1;
        } else if (tag.length() > 8 || tag.length() < 8) {
            counter_check = 1;
            return 1;
        } else {
            int length = tag.trim().length();
            int counter = 0;
            for (int i = 0; i < length; i++) {
                char character = tag.charAt(i);
                int ascii = (int) character;

                if (Character.isAlphabetic(character) || (character == '.') || (ascii >= 33 && ascii <= 47) || (ascii >= 58 && ascii <= 64) || (ascii >= 91 && ascii <= 96) || (ascii >= 123 && ascii <= 126)) {
                    counter++;
                    break;
                }
                if (Character.isDigit(character)) {
                    int a = (int) character;
                    int b = tag.indexOf(0);
                    if (a == 48 && b == 0) {
                        counter++;
                        break;
                    }
                } else {
                    counter = 0;
                }
            }

            if (counter > 0) {
                counter_check = 1;
                return 1;
            }
        }
        return 0;
    }

    public int validateAge(String age) {
        int counter_check = 0;
        if (age.trim().length() == 0) {
            counter_check = 1;
            return 1;
        } else if (age.length() > 3) {
            counter_check = 1;
            return 1;
        } else {
            int length = age.trim().length();
            int counter = 0;
            char character;
            char nextCharacter;
            for (int i = 0; i < length; i++) {

                character = age.charAt(i);

                int ascii = (int) character;

                if (Character.isAlphabetic(character) || (character == '.') || (ascii >= 33 && ascii <= 47) || (ascii >= 58 && ascii <= 64) || (ascii >= 91 && ascii <= 96) || (ascii >= 123 && ascii <= 126)) {
                    counter++;
                    break;
                }
                if (Character.isDigit(character)) {
                    int a = (int) character;
                    int b = age.indexOf(0);
                    if (a == 48 && b == 0) {
                        counter++;
                        break;
                    }
                } else {
                    counter = 0;
                }
            }

            if (counter > 0) {
                counter_check = 1;
                return 1;
            }
        }
        return 0;
    }

    public int validateNumber(String number) {

        int counter_check = 0;
        if (number.trim().length() == 0) {
            counter_check = 1;
            return 1;
        } else {
            int length = number.trim().length();
            int counter = 0;
            char character;
            char nextCharacter;
            for (int i = 0; i < length; i++) {

                character = number.charAt(i);

                int ascii = (int) character;

                if (Character.isAlphabetic(character) || (character == '.') || (ascii >= 33 && ascii <= 47) || (ascii >= 58 && ascii <= 64) || (ascii >= 91 && ascii <= 96) || (ascii >= 123 && ascii <= 126)) {
                    counter++;
                    break;
                }
                if (Character.isDigit(character)) {
                    int a = (int) character;
                    int b = number.indexOf(0);
                    if (a == 48 && b == 0) {
                        counter++;
                        break;
                    }
                } else {
                    counter = 0;
                }
            }

            if (counter > 0) {
                counter_check = 1;
                return 1;
            }
        }
        return 0;
    }

    public int validateStreet(String s) {
        int counter_check = 0;
        if (s.length() == 0) {
            counter_check = 1;
            return 1;
        } else {
            int length = s.trim().length();
            int counter = 0;
            for (int i = 0; i < length; i++) {
                char character = s.charAt(i);
                int ascii = (int) character;

                if ((character == '.') || (ascii >= 33 && ascii <= 47) || (ascii >= 58 && ascii <= 64) || (ascii >= 91 && ascii <= 96) || (ascii >= 123 && ascii <= 126)) {
                    counter++;
                    break;
                } else {
                    counter = 0;
                }
            }
            if (counter > 0) {
                counter_check = 1;
                return 1;
            }
        }
        return 0;
    }

    public int validateTiming(String timing) {
        int counter_check = 0;
        if (timing.trim().length() == 0) {
            counter_check = 1;
            return 1;
        }
        if (timing.trim().length() > 11 && timing.trim().length() < 11) {
            counter_check = 1;
            return 1;
        } else {

            int length = timing.trim().length();
            int counter = 0;
            for (int i = 0; i < length; i++) {
                char character = timing.charAt(i);
                int ascii = (int) character;

                if (!(timing.contains("AM") && (timing.contains("PM")) && (timing.contains("-"))) && !(timing.contains("AM") && (timing.contains("AM")) && (timing.contains("-"))) && !(timing.contains("PM") && (timing.contains("PM")) && (timing.contains("-")))) {

                    counter++;
                    break;
                }
                if (timing.contains("00")) {
                    counter++;
                    break;
                } else if (timing.contains("01") && timing.contains("02") && timing.contains("03") && timing.contains("04") && timing.contains("05") && timing.contains("06") && timing.contains("07") && timing.contains("08") && timing.contains("09") && timing.contains("10") && timing.contains("11") && timing.contains("12")) {
                    counter++;
                    break;
                } else {
                    counter = 0;
                }
            }
            if (counter > 0) {
                counter_check = 1;
                return 1;
            }
        }
        return 0;
    }
}
